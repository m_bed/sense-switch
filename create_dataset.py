from data import PennDatasetLoader, BigsDatasetLoader, VTDatasetLoader
from data.utilities.load_functions import load_hatt_dataset, load_bigs_dataset, load_vt_dataset

# CONFIG_FILE = "./configs/vt_dataset.json"
# TRAIN_PICKLE = "./datasets/VT/train.pickle"
# TEST_PICKLE = "./datasets/VT/test.pickle"
# loader = VTDatasetLoader.from_json(CONFIG_FILE)
# train_data, test_data = loader.load()
# loader.save(train_data, TRAIN_PICKLE)
# loader.save(test_data, TEST_PICKLE)
# train_ds, train_labels, test_ds, test_labels = load_vt_dataset(CONFIG_FILE, TRAIN_PICKLE, TEST_PICKLE, use_front_0=True,
#                                                                use_front_1=True, use_tactile=True, use_left_0=True,
#                                                                use_left_1=True)

#CONFIG_FILE = "./configs/bigs_dataset.json"
#TRAIN_PICKLE = "./datasets/BiGS/train.pickle"
#TEST_PICKLE = "./datasets/BiGS/test.pickle"
#loader = BigsDatasetLoader.from_json(CONFIG_FILE)
#train_data, test_data = loader.load()
#loader.save(train_data, TRAIN_PICKLE)
#loader.save(test_data, TEST_PICKLE)
#train_ds, train_labels, test_ds, test_labels = load_bigs_dataset(CONFIG_FILE, TRAIN_PICKLE, TEST_PICKLE, use_raw=True)

CONFIG_FILE = "./configs/penn_dataset.json"
TRAIN_PICKLE = "./datasets/PennHapticTex/train_100.pickle"
TEST_PICKLE = "./datasets/PennHapticTex/test_100.pickle"
loader = PennDatasetLoader.from_json(CONFIG_FILE)
train_data, test_data = loader.load()
loader.save(train_data, TRAIN_PICKLE)
loader.save(test_data, TEST_PICKLE)
train_ds, train_labels, test_ds, test_labels = load_hatt_dataset(CONFIG_FILE, TRAIN_PICKLE, TEST_PICKLE,
                                                                 use_force=True,
                                                                 use_accel=True,
                                                                 use_images=True)

print("aaaaaaaa")
