import os
import sys
from argparse import ArgumentParser

from sklearn.preprocessing import MultiLabelBinarizer
from skmultilearn.model_selection import IterativeStratification

from data import *
from data.utilities import load_bigs_dataset, load_hatt_dataset, save_dict
from data.utilities.load_functions import load_vt_dataset
from nn import LMF, MidFusion, allow_memory_growth, train, validate, train_multilabel, validate_multilabel
from nn.instances.LateFusion import LateFusion
from nn.instances.LateSupervisionFusion import LateSupervisionFusion
from nn.utilities.noise_functions import random_noise

tf.random.set_seed(0)

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
DEBUG = False


def cross_validate(args):
    multilabel = False
    class_names = None
    classes_idx = None
    main_mod = None

    if args.model_type.startswith("hatt"):
        x_train, y_train, x_test, y_test, class_names_full = load_hatt_dataset(args.data_loader_config,
                                                                               args.data_path_train,
                                                                               args.data_path_test,
                                                                               use_force=True,
                                                                               use_accel=True,
                                                                               use_vel=True,
                                                                               normalize=True)

        # # training without main modality - acceleration
        # x_train, y_train, x_test, y_test, class_names_full = load_hatt_dataset(args.data_loader_config,
        #                                                                        args.data_path_train,
        #                                                                        args.data_path_test,
        #                                                                        use_force=True,
        #                                                                        use_accel=False,
        #                                                                        use_vel=True,
        #                                                                        normalize=True)

        main_mod = 1
        parse_fun = hatt_img_timeserie_parse_function_aug
        class_names = np.unique(class_names_full)
        num_classes = len(np.unique(y_train))
    elif args.model_type.startswith("bigs"):
        x_train, y_train, x_test, y_test, class_names_full = load_bigs_dataset(args.data_loader_config,
                                                                               args.data_path_train,
                                                                               args.data_path_test,
                                                                               use_forces=True,
                                                                               use_prioprioception=True,
                                                                               normalize=True)

        # # training without main modality - force
        # x_train, y_train, x_test, y_test, class_names_full = load_bigs_dataset(args.data_loader_config,
        #                                                                        args.data_path_train,
        #                                                                        args.data_path_test,
        #                                                                        use_forces=False,
        #                                                                        use_prioprioception=True,
        #                                                                        normalize=True)

        main_mod = 2
        parse_fun = bigs_raw_electrodes_parse_function
        class_names = np.unique(class_names_full)
        num_classes = len(np.unique(y_train))
    elif args.model_type.startswith("vt"):
        x_train, y_train, x_test, y_test = load_vt_dataset(args.data_loader_config, args.data_path_train,
                                                           args.data_path_test,
                                                           use_front_0=False, use_front_1=True,
                                                           use_left_0=False, use_left_1=True,
                                                           use_tactile=True,
                                                           normalize=True)

        parse_fun = vt_img_timeserie_parse_function_aug
        num_classes = len(np.unique(y_train))

    elif args.model_type.startswith("bolt"):
        x_train, y_train, x_test, y_test, class_names_full = load_bolt_dataset(args.data_loader_config,
                                                                               args.data_path_train,
                                                                               args.data_path_test, use_images=True,
                                                                               use_electrodes=True,
                                                                               use_pdc=False, use_pac=False,
                                                                               use_tdc=False, use_tac=False,
                                                                               normalize=True)
        main_mod = 0
        # # training without main modality - images
        # x_train, y_train, x_test, y_test, class_names_full = load_bolt_dataset(args.data_loader_config,
        #                                                                        args.data_path_train,
        #                                                                        args.data_path_test, use_images=False,
        #                                                                        use_electrodes=True,
        #                                                                        use_pdc=False, use_pac=False,
        #                                                                        use_tdc=False, use_tac=False,
        #                                                                        normalize=True)
        # parse_fun = bigs_raw_electrodes_parse_function

        parse_fun = bolt_raw_electrodes_with_images_parse_function_aug
        multilabel = True
        class_names = np.unique(np.hstack(class_names_full))
        classes_idx = np.unique(np.hstack(y_train))
        num_classes = len(classes_idx)

    else:
        print("No known dataset loaded.")
        sys.exit(1)

    os.makedirs(args.results, exist_ok=True)
    save_dict(args, os.path.join(args.results, 'training_args.txt'))

    # split a dataset in a balanced way between classes
    # http://scikit.ml/api/skmultilearn.model_selection.iterative_stratification.html
    kf = IterativeStratification(n_splits=args.num_splits, order=1)

    # binarize multilabel data, otherwise use categorical values
    if multilabel:
        mlb = MultiLabelBinarizer()
        mlb.set_params(classes=classes_idx)
        y_train = mlb.fit_transform(y_train)
        y_test = mlb.fit_transform(y_test)

    # run stratified k-fold cross validation
    for split_no, (train_idx, val_idx) in enumerate(kf.split(X=x_train[0], y=y_train)):
        if DEBUG:
            print("Data balance between classes:")
            train_pick_x, train_pick_y = [x[train_idx] for x in x_train], y_train[train_idx]
            val_pick_x, val_pick_y = [x[val_idx] for x in x_train], y_train[val_idx]
            train_pick_y = train_pick_y.sum(axis=0)
            val_pick_y = val_pick_y.sum(axis=0)
            print(train_pick_y)
            print(val_pick_y)
            print("\n\n")
            continue

        if args.split_included_idx != -1 and split_no != args.split_included_idx:
            print("Passed {} split.".format(split_no))
            continue

        logs_path = os.path.join(args.results, '{}'.format(split_no))
        print("Cross-validation, split no. {}. Saving dataset samples indexes...".format(split_no))
        np.savetxt(logs_path + "{}_split_train_data_samples.txt".format(split_no), train_idx)
        np.savetxt(logs_path + "{}_split_val_data_samples.txt".format(split_no), val_idx)
        print("... saved.")

        # choose a model type
        net_args = [args.batch_size, num_classes, args.model_type]
        if args.model_type.endswith("tensor"):
            model = LMF(*net_args)
        elif args.model_type.endswith("mid"):
            model = MidFusion(*net_args)
        elif args.model_type.endswith("late"):
            model = LateFusion(*net_args)
        elif args.model_type.endswith("late_super"):
            model = LateSupervisionFusion(*net_args)
        else:
            print("No known model loaded.")
            sys.exit(1)

        # save network parameters
        save_dict(model.mod_dict, os.path.join(args.results, 'network_params.txt'))

        # setup optimization procedure
        eta = tf.Variable(args.lr)
        eta_value = tf.keras.optimizers.schedules.ExponentialDecay(args.lr, 1000, 0.99)
        eta.assign(eta_value(0))
        optimizer = tf.keras.optimizers.Adam(eta)
        ckpt = tf.train.Checkpoint(optimizer=optimizer, model=model)

        # restore from checkpoint
        if args.restore:
            path = tf.train.latest_checkpoint(logs_path)
            ckpt.restore(path)
        ckpt_man = tf.train.CheckpointManager(ckpt, logs_path, max_to_keep=10)

        # setup writers
        os.makedirs(logs_path, exist_ok=True)
        train_writer = tf.summary.create_file_writer(logs_path + "/train")
        val_writer = tf.summary.create_file_writer(logs_path + "/val")
        test_writer = tf.summary.create_file_writer(logs_path + "/test")

        # get a fold
        train_pick_x, train_pick_y = [x[train_idx] for x in x_train], y_train[train_idx]
        val_pick_x, val_pick_y = [x[val_idx] for x in x_train], y_train[val_idx]
        data_to_use_train = (*train_pick_x, train_pick_y)
        data_to_use_val = (*val_pick_x, val_pick_y)
        data_to_use_test = (*x_test, y_test)

        # create tf datasets
        train_ds = tf.data.Dataset.from_tensor_slices(data_to_use_train) \
            .map(parse_fun, 8) \
            .batch(args.batch_size) \
            .shuffle(x_train[0].shape[0])

        val_ds = tf.data.Dataset.from_tensor_slices(data_to_use_val) \
            .map(parse_fun, 8) \
            .batch(args.batch_size)

        test_ds = tf.data.Dataset.from_tensor_slices(data_to_use_test) \
            .map(parse_fun, 8) \
            .batch(args.batch_size)

        train_step, val_step, test_step = 0, 0, 0

        noise_func = None
        if args.add_noise_to_main_mod and main_mod not in [-1, None]:
            noise_func = random_noise

        # run a training procedure
        best_metric = 0.0
        for epoch in range(args.epochs):

            if multilabel:
                train_step = train_multilabel(model, train_writer, train_ds, optimizer, train_step, class_names,
                                              main_mod=main_mod, noise_fun=noise_func)
                val_step, _, _ = validate_multilabel(model, val_writer, val_ds, val_step, None, class_names)
                test_step, save_model, best_metric = validate_multilabel(model, test_writer, test_ds, test_step,
                                                                         best_metric,
                                                                         class_names,
                                                                         is_print=True,
                                                                         prefix="test")
            else:
                train_step = train(model, train_writer, train_ds, optimizer, train_step, class_names,
                                   main_mod=main_mod, noise_fun=noise_func)
                val_step, _, _ = validate(model, val_writer, val_ds, val_step, None, class_names)
                test_step, save_model, best_metric = validate(model, test_writer, test_ds, test_step,
                                                              best_metric,
                                                              class_names,
                                                              is_print=True,
                                                              prefix="test")

            # assign eta and reset the metrics for the next epoch
            eta.assign(eta_value(0))

            # save each save_period
            if save_model:
                ckpt_man.save()

        # end training on one, specified fold
        if args.split_included_idx != -1:
            break


if __name__ == '__main__':
    available_model_types = ['bigs_tensor', 'bigs_mid', 'bigs_late', 'bigs_late_super',
                             'hatt_tensor', 'hatt_mid', 'hatt_late', 'hatt_late_super',
                             'vt_tensor', 'vt_mid', 'vt_late', 'vt_late_super',
                             'bolt_tensor', 'bolt_mid', 'bolt_late', 'bolt_late_super']

    parser = ArgumentParser()
    parser.add_argument('--model-type', type=str, required=True,
                        choices=available_model_types)

    parser.add_argument('--data-loader-config', type=str, required=True)
    parser.add_argument('--data-path-train', type=str, required=True)
    parser.add_argument('--data-path-test', type=str, required=True)
    parser.add_argument('--results', type=str, default="cross_validation/logs")
    parser.add_argument('--batch-size', type=int, default=128)
    parser.add_argument('--latent-vec-size', type=int, default=100)
    parser.add_argument('--epochs', type=int, default=200)
    parser.add_argument('--lr', type=float, default=1e-3)
    parser.add_argument('--num-splits', type=int, default=5)
    parser.add_argument('--restore', default=False, action='store_true')
    parser.add_argument('--add-noise-to-main-mod', default=False, action='store_true')
    parser.add_argument('--split-included-idx', type=int, default=-1)

    args, _ = parser.parse_known_args()

    if args.model_type not in available_model_types:
        parser.print_help()
        sys.exit(1)

    allow_memory_growth()
    cross_validate(args)
