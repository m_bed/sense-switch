import numpy as np


def split_equally_between_classes(data: dict, labels: np.array, train_split=0.8, labels_key="labels"):
    values = np.unique(labels)
    modalities_train, modalities_test = dict(), dict()

    for mod_key in data:
        modality_signals = np.array(data[mod_key])
        train_dataset_x, train_dataset_y = list(), list()
        test_dataset_x, test_dataset_y = list(), list()

        for i, val in enumerate(values):
            arr = np.where(labels == val, 1, 0)
            idx = np.argwhere(arr == 1)[:, 0]

            split_num = int(train_split * idx.shape[0])

            idx_train, idx_test = idx[:split_num], idx[split_num:]
            train_dataset_x.append(modality_signals[idx_train])
            train_dataset_y.append(labels[idx_train])
            test_dataset_x.append(modality_signals[idx_test])
            test_dataset_y.append(labels[idx_test])

        modalities_train[mod_key] = np.concatenate(train_dataset_x, 0)
        modalities_test[mod_key] = np.concatenate(test_dataset_x, 0)

        # add labels just once
        if labels_key not in modalities_train.keys() and labels_key not in modalities_test.keys():
            modalities_train[labels_key] = np.concatenate(train_dataset_y, 0)
            modalities_test[labels_key] = np.concatenate(test_dataset_y, 0)

    return modalities_train, modalities_test
