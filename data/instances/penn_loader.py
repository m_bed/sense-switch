import glob
import os
import xml.etree.ElementTree as ET

import h5py
import numpy as np

from .utils import split_equally_between_classes
from ..loaders import ImgaeForceLoader


class PennDatasetLoader(ImgaeForceLoader):
    CLASSES = 100

    def load(self):
        """
        Each class is described by 4 files: Accel*.xml, Force*.xml and Position*.xml and CLASS_NAME.bmp
        :return: dataset
        """
        files = self._get_filenames()

        # prepare dataset
        acc, force, pos, lab_num = list(), list(), list(), list()
        files = np.array(files, dtype=str)
        for i, group in enumerate(files):
            assert len(group) == 5
            a, f, p, class_id = self._read_xml(group[-3:])
            acc.append(np.array(a).reshape((-1, self.sample_length)))
            force.append(np.array(f).reshape((-1, self.sample_length)))
            pos.append(np.array(p).reshape((-1, self.sample_length)))
            lab_num.append(i)

        # reshape dataset to NxSAMPLE_LENGTHxC
        acc = self._squeeze_2d_to_3d(acc)[..., np.newaxis]
        force = self._squeeze_2d_to_3d(force)[..., np.newaxis]
        pos = self._squeeze_2d_to_3d(pos)[..., np.newaxis]

        # replicate paths to fit NxSAMPLE_LENGTHx1
        num = pos.shape[0]
        lab_num = self._resize_along(np.array(lab_num), num)[..., np.newaxis]
        imgs_paths = self._resize_along(np.array(files[:, 1]), num)[..., np.newaxis]

        # SPLIT EQUALLY BETWEEN CLASSES

        data = {
            self.IMG_PATHS: imgs_paths,
            self.ACCELERATIONS: acc,
            self.FORCES: force,
            self.POSITIONS: pos
        }
        train_data, test_data = split_equally_between_classes(data, lab_num, self.split_train, self.LABELS_NUM)

        train_data[self.IMG_PATHS] = train_data[self.IMG_PATHS].flatten()
        test_data[self.IMG_PATHS] = test_data[self.IMG_PATHS].flatten()

        return train_data, test_data

    def _squeeze_2d_to_3d(self, arr):
        arr = np.array(arr)
        length = arr.shape[-1]
        arr = arr.transpose((2, 0, 1))
        arr = arr.reshape((length, -1))
        arr = arr.transpose((1, 0))
        return arr

    def _get_filenames(self):
        path_signals = self.data_folder.rstrip('/') + self.files_regex
        path_images = self.data_folder.rstrip('/') + self.images_regex
        signals = sorted(glob.glob(path_signals))
        images = sorted(glob.glob(path_images))
        assert len(signals) == 3 * len(images)  # assert three signals per class

        # save CLASS_ID/CLASS_NAME/IMAGE_PATH/FORCE_PATH/ACCELERATION_PATH/POSITION_PATH
        files = list()
        for n in range(0, len(signals), 3):
            image_path = images[int(n / 3)]
            class_name = image_path.rsplit('/')[-1][:-4]  # remove .bmp from image path
            class_paths = [class_name, image_path, signals[n], signals[n + 1], signals[n + 2]]
            files.append(class_paths)
        return files

    def _resize_along(self, arr, times=1):
        arr = arr[..., np.newaxis]
        arr = np.tile(arr, int(times / arr.shape[0]))
        arr = arr.reshape(-1)
        return arr

    def _read_xml(self, files):
        a, f, p = list(), list(), list()
        is_class_id = False
        id = None
        for file in files:

            tree = ET.parse(str(file), ET.XMLParser(encoding="utf-8"))
            root = tree.getroot()

            # read class id
            if not is_class_id:
                id = int(root.get('texture'))
                is_class_id = True

            # read data
            if 'Accel' in file:
                a = self._get_tag(root, "Accel")
            if 'Force' in file:
                f = self._get_tag(root, "ForceNormal")
            if 'Position' in file:
                p = self._get_tag(root, "Speed")

        return a, f, p, id

    def _get_tag(self, root, tag) -> list:
        values = list()
        if self.frequency_divider < 1:
            self.frequency_divider = 1
        for element in root.iter(tag):
            i = 0
            for value in element:
                if self.frequency_divider >= 1 and i % self.frequency_divider == 0:
                    val = float(value.text)
                    values.append(val)
                i += 1
        return values


underrepresented_adjectives = ['gritty', 'crinkly', 'metallic', 'nice', 'unpleasant', 'thin', 'sticky']


class PennBoltDatasetLoader(ImgaeForceLoader):
    CLASSES = 24
    IMG_SUFFIX = ".h5.jpg"
    IMG_FOLDER = "images_bak"

    def names2numbers(self, adjectives: list or tuple, num_str_dict: dict):
        l = list()
        for adj in adjectives:
            if adj in num_str_dict.keys():
                l.append(num_str_dict[adj])
        return l

    def get_signals(self, path):
        electrodes_0 = list()
        pdc_0 = list()
        pac_0 = list()
        tdc_0 = list()
        tac_0 = list()
        electrodes_1 = list()
        pdc_1 = list()
        pac_1 = list()
        tdc_1 = list()
        tac_1 = list()
        adjectives = list()
        adjectives_numbers = list()
        names = list()

        with h5py.File(path, 'r') as f:
            base_keys = list(f.keys())[2:]

            # remove chosen adjectives
            a = np.array(f["adjectives"])
            adjstr, _ = np.unique(a, return_inverse=True)
            for i, s in enumerate(adjstr):
                if s in underrepresented_adjectives:
                    adjstr = adjstr[adjstr != s]

            names_dict = dict(zip(adjstr, np.linspace(0, adjstr.shape[0], adjstr.shape[0], dtype=np.int32)))

            # gather data
            for i, sample_key in enumerate(base_keys):
                sample = f[sample_key]

                if sample_key[-3:] not in ["_09", "_10"] and sample_key not in ["train_test_negative_sets",
                                                                                "train_test_sets"]:
                    adj_set = np.array(f[sample_key]["adjectives"])
                    adj = [str(adj)[2:-1] for adj in adj_set]

                    # no idea why it should be removed, but there is no gritty in the comparing paper
                    # others are really underrepresented in the dataset (at least 30 occurances set)
                    for ua in underrepresented_adjectives:
                        if ua in adj:
                            adj.remove(ua)

                    num = self.names2numbers(adj, names_dict)
                    adjectives.append(adj)
                    adjectives_numbers.append(num)
                    names.append(sample_key)

                    electrodes_0.append(np.array(sample['biotacs']['finger_0']['electrodes']))
                    pdc_0.append(np.array(sample['biotacs']['finger_0']['pdc']))
                    pac_0.append(np.array(sample['biotacs']['finger_0']['pac']))
                    tdc_0.append(np.array(sample['biotacs']['finger_0']['tdc']))
                    tac_0.append(np.array(sample['biotacs']['finger_0']['tac']))

                    electrodes_1.append(np.array(sample['biotacs']['finger_1']['electrodes']))
                    pdc_1.append(np.array(sample['biotacs']['finger_1']['pdc']))
                    pac_1.append(np.array(sample['biotacs']['finger_1']['pac']))
                    tdc_1.append(np.array(sample['biotacs']['finger_1']['tdc']))
                    tac_1.append(np.array(sample['biotacs']['finger_1']['tac']))

        # numerate classes by integers
        return (electrodes_0, pdc_0, pac_0, tdc_0, tac_0), \
               (electrodes_1, pdc_1, pac_1, tdc_1, tac_1), adjectives, adjectives_numbers, names

    @staticmethod
    def _remove_from_indexes_list(indexes, my_lists):
        for index in sorted(indexes, reverse=True):
            for l in my_lists:
                if index < len(l):
                    del l[index]
        return my_lists

    def pad(self, signal, length):
        diff0 = signal.shape[0] - length
        if diff0 < 0:
            diff = abs(diff0)
            pad = np.zeros(shape=diff) if len(signal.shape) < 2 else np.zeros(shape=(diff, signal.shape[-1]))
            signal = np.concatenate([signal, pad], axis=0)
        elif diff0 >= 0:
            signal = signal[:length, ...]
        else:
            raise ValueError("Biotac electrode signals have unequal lengths.")

        return signal

    def load(self):

        # load all data from images and signals
        full_signals_0, full_signals_1, full_adjectives, full_adjectives_numbers, full_names = self.get_signals(
            self.data_folder.rstrip('/') + self.files_regex)

        # combine img paths with signals
        img_files = list()
        idx_to_remove = list()
        for i, image_name in enumerate(full_names):
            img_path = os.path.join(self.data_folder.rstrip('/'), self.IMG_FOLDER, image_name + self.IMG_SUFFIX)

            if os.path.isfile(img_path):
                img_files.append(img_path)
            else:
                idx_to_remove.append(i)

        # remove indexes that does not have corresponding image
        lists = (*full_signals_0, *full_signals_1, full_adjectives, full_adjectives_numbers, full_names)
        electrodes_0, pdc_0, pac_0, tdc_0, tac_0, electrodes_1, pdc_1, pac_1, tdc_1, tac_1, full_adjectives, full_adjectives_numbers, full_names = self._remove_from_indexes_list(
            idx_to_remove, lists)

        # downsample signals
        min_length = 9999999999.9
        for i in range(len(electrodes_0)):
            electrodes_0[i] = electrodes_0[i][::self.frequency_divider]
            pdc_0[i] = pdc_0[i][::self.frequency_divider]
            pac_0[i] = pac_0[i][::self.frequency_divider]
            tdc_0[i] = tdc_0[i][::self.frequency_divider]
            tac_0[i] = tac_0[i][::self.frequency_divider]

            electrodes_1[i] = electrodes_1[i][::self.frequency_divider]
            pdc_1[i] = pdc_1[i][::self.frequency_divider]
            pac_1[i] = pac_1[i][::self.frequency_divider]
            tdc_1[i] = tdc_1[i][::self.frequency_divider]
            tac_1[i] = tac_1[i][::self.frequency_divider]

            min_length_new = np.min((electrodes_0[i].shape[0], electrodes_1[i].shape[0],
                                     pac_0[i].shape[0], pdc_1[i].shape[0],
                                     pac_0[i].shape[0], pac_1[i].shape[0],
                                     tdc_0[i].shape[0], tdc_1[i].shape[0],
                                     tac_0[i].shape[0], tac_1[i].shape[0]))

            if min_length_new < min_length:
                min_length = min_length_new

        # crop signals and pad with zeros to the max size
        signals = (electrodes_0, pdc_0, pac_0, tdc_0, tac_0, electrodes_1, pdc_1, pac_1, tdc_1, tac_1)
        # for signal in signals:
        #     for i in range(len(signal)):
        #         signal[i] = self.pad(signal[i], length)

        for signal in signals:
            for i in range(len(signal)):
                signal[i] = signal[i][:min_length]

        ds = {
            self.IMG_PATHS: img_files,
            self.ELECTRODES + "_0": np.array(electrodes_0),
            self.PAC + "_0": np.array(pac_0),
            self.PDC + "_0": np.array(pdc_0)[..., np.newaxis],
            self.TAC + "_0": np.array(tac_0)[..., np.newaxis],
            self.TDC + "_0": np.array(tdc_0)[..., np.newaxis],
            self.ELECTRODES + "_1": np.array(electrodes_1),
            self.PAC + "_1": np.array(pac_1),
            self.PDC + "_1": np.array(pdc_1)[..., np.newaxis],
            self.TAC + "_1": np.array(tac_1)[..., np.newaxis],
            self.TDC + "_1": np.array(tdc_1)[..., np.newaxis],
            self.LABELS_STR: full_adjectives,
            self.LABELS_NUM: full_adjectives_numbers
        }

        return ds
