import glob

import numpy as np
from scipy.io import loadmat
from scipy.signal import resample
from .utils import split_equally_between_classes
from ..loaders import GraspStabilityLoader

import matplotlib.pyplot as plt


class BigsDatasetLoader(GraspStabilityLoader):
    CLASSES = 2

    def load(self):
        """
        Each class is described by a *.mat file
        :return: dataset
        """
        files = self._get_filenames()

        forces, torques, hand_poses, hand_orientations = list(), list(), list(), list()
        pressures_rf, pressures_lf, pressures_mf = list(), list(), list()
        raws_rf, raws_lf, raws_mf = list(), list(), list()
        success = list()
        maximal_num_samples = self._find_max_num_samples(files)
        for i, file in enumerate(files):
            data = loadmat(file)

            # resample signals to fit one time-period
            pressure_rf = resample(data["RF_PDC"], num=maximal_num_samples)
            pressure_lf = resample(data["LF_PDC"], num=maximal_num_samples)
            pressure_mf = resample(data["MF_PDC"], num=maximal_num_samples)
            force = resample(data["HAND_FORCE"], num=maximal_num_samples)
            torque = resample(data["HAND_TORQUE"], num=maximal_num_samples)
            hand_pose = resample(data["HAND_POSITION"], num=maximal_num_samples)
            hand_orientation = resample(data["HAND_ORIENTATION"], num=maximal_num_samples)
            raw_rf = resample(data["RF_ELECTRODES"], num=maximal_num_samples)
            raw_lf = resample(data["LF_ELECTRODES"], num=maximal_num_samples)
            raw_mf = resample(data["MF_ELECTRODES"], num=maximal_num_samples)

            # append values to lists
            forces.append(force)
            torques.append(torque)
            hand_poses.append(hand_pose)
            hand_orientations.append(hand_orientation)
            pressures_lf.append(pressure_lf)
            pressures_mf.append(pressure_mf)
            pressures_rf.append(pressure_rf)
            raws_lf.append(raw_lf)
            raws_mf.append(raw_mf)
            raws_rf.append(raw_rf)
            success.append(int(bool("success" in file)))

        # SPLIT EQUALLY BETWEEN CLASSES
        data = {
            self.FORCES: np.array(forces),
            self.TORQUES: np.array(torques),
            self.HAND_ORIENTATIONS: np.array(hand_orientations),
            self.HAND_POSITIONS: np.array(hand_poses),
            self.PRESSURE_LF: np.array(pressures_lf),
            self.PRESSURE_MF: np.array(pressures_mf),
            self.PRESSURE_RF: np.array(pressures_rf),
            self.RAW_LF: np.array(raws_lf),
            self.RAW_MF: np.array(raws_mf),
            self.RAW_RF: np.array(raws_rf),
        }

        train_data, test_data = split_equally_between_classes(data, np.array(success)[..., np.newaxis], self.split_train, self.LABELS)

        return train_data, test_data

    def _find_max_num_samples(self, files):
        maximal = 0
        for i, file in enumerate(files):
            current = loadmat(file)["RF_PDC"].shape[0]
            if i > 0:
                if current > maximal:
                    maximal = current
            else:
                maximal = current
        return maximal

    def _frequency_equalizer(self, signal, target_len):
        skip = np.int(np.ceil(signal.shape[0] / target_len))
        downsampled = signal[::skip, :]
        downsampled_cut = downsampled[-target_len:]

        if signal.shape[0] >= target_len:
            skip = np.int(np.ceil(signal.shape[0] / target_len))
            to_pad_shape = skip * target_len
            padded = np.zeros((to_pad_shape, signal.shape[1]))
            padded[-signal.shape[0]:, :] = signal
            padded = padded[::skip, :]
        else:
            padded = np.zeros((target_len, signal.shape[1]))
            padded[-signal.shape[0]:, :] = signal

        return padded

    def _get_filenames(self):
        path_signals = self.data_folder.rstrip('/') + self.files_regex
        files = sorted(glob.glob(path_signals))
        return files
