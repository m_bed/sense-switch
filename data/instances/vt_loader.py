import glob
import os
import xml.etree.ElementTree as ET

import h5py
import numpy as np
from .utils import split_equally_between_classes

from ..loaders import ImgaeForceLoader, ImageTactileLoader

import matplotlib.pyplot as plt


class VTDatasetLoader(ImageTactileLoader):
    CLASSES = 6

    def load(self):
        """
        Each class is described by 4 files: Accel*.xml, Force*.xml and Position*.xml and CLASS_NAME.bmp
        :return: dataset
        """
        file_paths = self._get_filenames()
        exe = list(file_paths.items())[0][1][0]
        data = {k: [] for k in exe.keys()}
        labels = []
        for i, (k, v) in enumerate(file_paths.items()):
            print(k, v)
            for obj in v:
                for obj_k, obj_v in obj.items():
                    data[obj_k].append(read_(obj_v))
                labels.append(i)

        train_data, test_data = split_equally_between_classes(data, np.array(labels), self.split_train, self.LABELS_NUM)

        #train_data[self.IMG_PATHS] = train_data[self.IMG_PATHS].flatten()
        #test_data[self.IMG_PATHS] = test_data[self.IMG_PATHS].flatten()

        return train_data, test_data

    def _get_filenames(self):
        path = self.data_folder
        data = {}
        for a in os.listdir(path):
            path_tmp_a = os.path.join(path, a)
            if os.path.isdir(path_tmp_a):
                data[a] = []
                for b in os.listdir(path_tmp_a):
                    path_tmp_b = os.path.join(path_tmp_a, b)
                    if os.path.isdir(path_tmp_b):
                        for c in os.listdir(path_tmp_b):
                            path_tmp_c = os.path.join(path_tmp_b, c)
                            if os.path.isdir(path_tmp_c):
                                for d in os.listdir(path_tmp_c):
                                    path_tmp_d = os.path.join(path_tmp_c, d)
                                    if os.path.isdir(path_tmp_d):
                                        record = {}
                                        for e in os.listdir(path_tmp_d):
                                            name = e.replace(".jpg", "").replace(".txt", "")
                                            record[name] = os.path.join(path_tmp_d, e)
                                        if len(record) == 7:
                                            data[a].append(record)
        return data


def read_(path):
    print(path)
    data = path
    if path.endswith(".txt"):
        data = np.loadtxt(path, delimiter=" ")
    return data
