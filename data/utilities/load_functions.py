import pickle

import numpy as np

from data import VTDatasetLoader
from data.instances import PennDatasetLoader, BigsDatasetLoader


def normalization(x_train, x_test):
    means = [np.mean(x_train[i], axis=(0, 1), keepdims=True) for i in range(len(x_train))]
    stddevs = [np.std(x_train[i], axis=(0, 1), keepdims=True) for i in range(len(x_train))]

    for i, (m, s) in enumerate(zip(means, stddevs)):
        x_train[i] = (x_train[i] - m) / s
        x_test[i] = (x_test[i] - m) / s

    return x_train, x_test


def save_dict(dict_to_save, path):
    with open(path, 'w') as f:
        d = dict_to_save.__dict__ if hasattr(dict_to_save, "__dict__") else dict_to_save
        for arg in d:
            argument_value = d[arg]
            f.write('{0} : {1}\n'.format(arg, argument_value))


def load_hatt_dataset(loader_config_path, pickle_train_path, pickle_test_path, use_images=False, use_force=False,
                      use_accel=False, use_vel=False, normalize=True):
    if True not in [use_force, use_accel, use_vel, use_images]:
        print("No values specified from the PennHapticTex dataset.")
        return None

    loader = PennDatasetLoader.from_json(loader_config_path)

    with open(pickle_train_path, "rb") as fp:
        train_dataset = pickle.load(fp)
    with open(pickle_test_path, "rb") as fp:
        test_dataset = pickle.load(fp)

    # pick data
    x_train = []
    x_test = []

    if use_force:
        x_train += [train_dataset[loader.FORCES]]
        x_test += [test_dataset[loader.FORCES]]

    if use_accel:
        x_train += [train_dataset[loader.ACCELERATIONS]]
        x_test += [test_dataset[loader.ACCELERATIONS]]

    if use_vel:
        x_train += [train_dataset[loader.POSITIONS]]
        x_test += [test_dataset[loader.POSITIONS]]

    # normalize signals in the dataset according to the train dataset
    if normalize:
        x_train, x_test = normalization(x_train, x_test)

    # add images in the end - filepaths
    if use_images:
        x_train += [train_dataset[loader.IMG_PATHS]]
        x_test += [test_dataset[loader.IMG_PATHS]]

    class_names = [path.rsplit('/')[-1][:-4] for path in train_dataset[loader.IMG_PATHS]]
    return x_train, train_dataset[loader.LABELS_NUM], x_test, test_dataset[loader.LABELS_NUM], class_names


def load_bigs_dataset(loader_config_path, pickle_train_path, pickle_test_path, use_raw=False, use_pressure=False,
                      use_prioprioception=False, use_torques=False,
                      use_forces=False, normalize=True):
    if True not in [use_forces, use_pressure, use_prioprioception, use_raw, use_torques]:
        print("No values specified from the BiGS dataset.")
        return None

    loader = BigsDatasetLoader.from_json(loader_config_path)

    with open(pickle_train_path, "rb") as fp:
        train_dataset = pickle.load(fp)
    with open(pickle_test_path, "rb") as fp:
        test_dataset = pickle.load(fp)

    # pick data
    x_train = []
    x_test = []
    if use_raw:
        x_train += [train_dataset[loader.RAW_LF], train_dataset[loader.RAW_MF], train_dataset[loader.RAW_RF]]
        x_test += [test_dataset[loader.RAW_LF], test_dataset[loader.RAW_MF], test_dataset[loader.RAW_RF]]

    if use_pressure:
        x_train += [train_dataset[loader.PRESSURE_LF], train_dataset[loader.PRESSURE_MF],
                    train_dataset[loader.PRESSURE_RF]]
        x_test += [test_dataset[loader.PRESSURE_LF], test_dataset[loader.PRESSURE_MF],
                   test_dataset[loader.PRESSURE_RF]]

    if use_torques:
        x_train += [train_dataset[loader.TORQUES]]
        x_test += [test_dataset[loader.TORQUES]]

    if use_prioprioception:
        x_train += [train_dataset[loader.HAND_POSITIONS], train_dataset[loader.HAND_ORIENTATIONS]]
        x_test += [test_dataset[loader.HAND_POSITIONS], test_dataset[loader.HAND_ORIENTATIONS]]

    if use_forces:
        x_train += [train_dataset[loader.FORCES]]
        x_test += [test_dataset[loader.FORCES]]

    # normalize signals in the dataset according to the train dataset
    if normalize:
        x_train, x_test = normalization(x_train, x_test)

    class_names = ["SUCCESS", "FAIL"]

    return x_train, train_dataset[loader.LABELS], x_test, test_dataset[loader.LABELS], class_names


def load_vt_dataset(loader_config_path, pickle_train_path, pickle_test_path, use_left_0=False, use_left_1=False,
                    use_front_0=False, use_front_1=False, use_tactile=False, use_pos=False, normalize=True):
    if True not in [use_left_0, use_left_1, use_front_0, use_front_1, use_tactile, use_pos]:
        print("No values specified from the VT dataset.")
        return None

    loader = VTDatasetLoader.from_json(loader_config_path)

    with open(pickle_train_path, "rb") as fp:
        train_dataset = pickle.load(fp)
    with open(pickle_test_path, "rb") as fp:
        test_dataset = pickle.load(fp)

    # pick data
    x_train = []
    x_test = []

    if use_tactile:
        x_train += [train_dataset["tactile"]]
        x_test += [test_dataset["tactile"]]

    if use_pos:
        x_train += [train_dataset["pos"]]
        x_test += [test_dataset["pos"]]

    # normalize signals in the dataset according to the train dataset
    if normalize:
        x_train, x_test = normalization(x_train, x_test)

    if use_left_0:
        x_train += [train_dataset["left_0"]]
        x_test += [test_dataset["left_0"]]

    if use_left_1:
        x_train += [train_dataset["left_1"]]
        x_test += [test_dataset["left_1"]]

    if use_front_0:
        x_train += [train_dataset["front_0"]]
        x_test += [test_dataset["front_0"]]

    if use_front_1:
        x_train += [train_dataset["front_1"]]
        x_test += [test_dataset["front_1"]]

    return x_train, train_dataset[loader.LABELS_NUM], x_test, test_dataset[loader.LABELS_NUM]


def load_bolt_dataset(loader_config_path, pickle_train_path, pickle_test_path, use_images=False,
                      use_electrodes=False, use_pdc=False, use_pac=False, use_tdc=False, use_tac=False, normalize=True):
    if True not in [use_images, use_electrodes, use_pdc, use_pac, use_tdc, use_tac]:
        print("No values specified from the BOLT dataset.")
        return None

    # Penn loader used only for the same dict keys
    loader = PennDatasetLoader.from_json(loader_config_path)

    with open(pickle_train_path, "rb") as fp:
        train_dataset = pickle.load(fp)
    with open(pickle_test_path, "rb") as fp:
        test_dataset = pickle.load(fp)

    # pick data
    x_train = []
    x_test = []

    if use_electrodes:
        x_train += [train_dataset[loader.ELECTRODES + "_0"]]
        x_train += [train_dataset[loader.ELECTRODES + "_1"]]
        x_test += [test_dataset[loader.ELECTRODES + "_0"]]
        x_test += [test_dataset[loader.ELECTRODES + "_1"]]

    if use_pdc:
        x_train += [train_dataset[loader.PDC + "_0"]]
        x_train += [train_dataset[loader.PDC + "_1"]]
        x_test += [test_dataset[loader.PDC + "_0"]]
        x_test += [test_dataset[loader.PDC + "_1"]]

    if use_pac:
        x_train += [train_dataset[loader.PAC + "_0"]]
        x_train += [train_dataset[loader.PAC + "_1"]]
        x_test += [test_dataset[loader.PAC + "_0"]]
        x_test += [test_dataset[loader.PAC + "_1"]]

    if use_tdc:
        x_train += [train_dataset[loader.TDC + "_0"]]
        x_train += [train_dataset[loader.TDC + "_1"]]
        x_test += [test_dataset[loader.TDC + "_0"]]
        x_test += [test_dataset[loader.TDC + "_1"]]

    if use_tac:
        x_train += [train_dataset[loader.TAC + "_0"]]
        x_train += [train_dataset[loader.TAC + "_1"]]
        x_test += [test_dataset[loader.TAC + "_0"]]
        x_test += [test_dataset[loader.TAC + "_1"]]

    # normalize signals in the dataset according to the train dataset
    if normalize:
        x_train, x_test = normalization(x_train, x_test)

    if use_images:
        x_train += [train_dataset[loader.IMG_PATHS]]
        x_test += [test_dataset[loader.IMG_PATHS]]

    train_labels = train_dataset[loader.LABELS_NUM].tolist() if isinstance(train_dataset[loader.LABELS_NUM], np.ndarray) \
        else train_dataset[loader.LABELS_NUM]
    test_labels = test_dataset[loader.LABELS_NUM].tolist() if isinstance(test_dataset[loader.LABELS_NUM], np.ndarray) \
        else test_dataset[loader.LABELS_NUM]
    class_names = test_dataset[loader.LABELS_STR].tolist() if isinstance(test_dataset[loader.LABELS_STR], np.ndarray) \
        else test_dataset[loader.LABELS_STR]

    return x_train, train_labels, x_test, test_labels, class_names
