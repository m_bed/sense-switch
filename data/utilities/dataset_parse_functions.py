import numpy as np
import tensorflow as tf


def crop_and_pad(timeseries, min_length):
    out = list()
    for i in range(len(timeseries)):
        arr = timeseries[i]
        if arr.shape[0] < min_length:
            pad = np.zeros(shape=[abs(arr.shape[0] - min_length), 3])
            arr = np.concatenate([arr, pad], axis=0)
        out.append(arr[:min_length, :])
    return np.array(out)


def augment_image(img):
    # img = tf.image.random_flip_left_right(img)
    img = tf.image.random_hue(img, 0.1)
    img = tf.image.random_saturation(img, 0.9, 1.1)
    img = tf.image.random_brightness(img, 0.1)
    img = tf.image.random_contrast(img, 0.9, 1.1)
    return img


def pad_signal_with_zeros(signals: list or tuple):
    max_signal_length = signals[0].shape[0]
    for elem in signals:
        current_signal_length = elem.shape[0]
        if current_signal_length > max_signal_length:
            max_signal_length = current_signal_length

    padded = list()
    num_channels = signals[0].shape[1]
    for signal in signals:
        zeros = np.zeros(shape=[max_signal_length, num_channels])
        zeros[-signal.shape[0]:, :] = signal
        padded.append(zeros)

    return padded


def tf_load_img(path, chan, width, height, decode_fun=tf.image.decode_png, img_type=tf.float32):
    image_string = tf.io.read_file(path)
    image = decode_fun(image_string, chan)

    if width is not None and height is not None:
        image = tf.image.resize(image, (width, height))

    if img_type is not None:
        image = tf.image.convert_image_dtype(image, img_type, saturate=False)

    return image


def hatt_img_timeserie_parse_function_aug(*args):
    label = tf.cast(args[-1], tf.int32)
    return args[:-1], label


def bigs_raw_electrodes_parse_function(*args):
    label = tf.cast(args[-1], tf.int32)
    return args[:-1], label


def vt_img_timeserie_parse_function_aug(*args):
    def load_img(img):
        x = tf_load_img(img, 3, 224, 224, decode_fun=tf.image.decode_jpeg)
        x = augment_image(x)
        x = tf.image.per_image_standardization(x)
        return x

    img1 = load_img(args[1])
    img2 = load_img(args[2])
    label = tf.cast(args[-1], tf.int32)
    return (args[0], img1, img2), label


def bolt_raw_electrodes_with_images_parse_function_aug(*args):
    x = tf_load_img(args[-2], 3, 244, 244, decode_fun=tf.image.decode_jpeg)
    x = augment_image(x)
    x = tf.image.per_image_standardization(x)
    label = tf.cast(args[-1], tf.int32)
    return (x, args[0], args[1]), label
