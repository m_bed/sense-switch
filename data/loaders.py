import json
import pickle


class Loader(object):
    def __init__(self):
        pass

    @classmethod
    def from_json(cls, json_path: str) -> 'Loader':
        raise NotImplementedError("Implement method of loading parameters specific to your dataset.")

    def load(self):
        raise NotImplementedError("Implement method of loading parameters specific to your dataset.")

    def save(self, dataset: dict, path: str):
        file = open(path, 'wb')
        pickle.dump(dataset, file)
        file.close()


class ImgaeForceLoader(Loader):
    FILES = "files_in_the_group"
    ID = 'id_of_the_class'
    ACCELERATIONS = "acc"
    FORCES = "forces"
    POSITIONS = "positions"
    LABELS_STR = "labels_str"
    LABELS_NUM = "labels_num"
    IMG_PATHS = "images_paths"
    ELECTRODES = "electrodes"
    PAC = "pac"
    PDC = "pad"
    TAC = "tac"
    TDC = "tad"

    def __init__(self, data_folder: str = "",
                 files_regex: str = "xml", pick_first_n_classes: int = 10,
                 frequency_divider: int = 10, sample_length: int = 100,
                 images_regex: str = "bmp", split_train: float = 0.8):
        super(ImgaeForceLoader, self).__init__()
        self.data_folder = data_folder
        self.files_regex = files_regex
        self.pick_first_n_classes = pick_first_n_classes
        self.frequency_divider = frequency_divider
        self.sample_length = sample_length
        self.images_regex = images_regex

        if 0.0 < split_train < 1.0:
            self.split_train = split_train
        else:
            print("split_train set to 0.8")
            self.split_train = 0.8

    @classmethod
    def from_json(cls, json_path: str) -> 'ImgaeForceLoader':
        with open(json_path, 'r') as f:
            params = json.load(f)
        return cls(data_folder=params["data_folder"], files_regex=params["files_regex"],
                   frequency_divider=params["frequency_divider"],
                   sample_length=params["sample_length"], images_regex=params["images_regex"],
                   split_train=params["split_train"])


class ImageTactileLoader(Loader):
    FILES = "files_in_the_group"
    ID = 'id_of_the_class'
    TACTILE = "tac"
    POSITIONS = "positions"
    LABELS_STR = "labels_str"
    LABELS_NUM = "labels_num"
    IMG_PATHS = "images_paths"

    def __init__(self, data_folder: str = "", sample_length: int = 100, split_train: float = 0.8):
        super(ImageTactileLoader, self).__init__()
        self.data_folder = data_folder
        self.sample_length = sample_length

        if 0.0 < split_train < 1.0:
            self.split_train = split_train
        else:
            print("split_train set to 0.8")
            self.split_train = 0.8

    @classmethod
    def from_json(cls, json_path: str) -> 'ImageTactileLoader':
        with open(json_path, 'r') as f:
            params = json.load(f)
        return cls(data_folder=params["data_folder"], sample_length=params["sample_length"],
                   split_train=params["split_train"])


class GraspStabilityLoader(Loader):
    TORQUES = "torque"
    FORCES = "forces"
    HAND_POSITIONS = "positions"
    HAND_ORIENTATIONS = "oreintations"
    PRESSURE_RF = "p_rf"
    PRESSURE_LF = "p_lf"
    PRESSURE_MF = "p_mf"
    RAW_LF = "raw_lf"
    RAW_MF = "raw_mf"
    RAW_RF = "raw_rf"

    LABELS = "labels"

    def __init__(self, data_folder: str = "",
                 files_regex: str = "xml",
                 common_frequency_hz: int = 10, sample_length: int = 100, split_train: float = 0.8):
        super(GraspStabilityLoader, self).__init__()
        self.data_folder = data_folder
        self.files_regex = files_regex
        self.common_frequency_hz = common_frequency_hz
        self.sample_length = sample_length

        if 0.0 < split_train < 1.0:
            self.split_train = split_train
        else:
            print("split_train set to 0.8")
            self.split_train = 0.8

    @classmethod
    def from_json(cls, json_path: str) -> 'GraspStabilityLoader':
        with open(json_path, 'r') as f:
            params = json.load(f)
        return cls(data_folder=params["data_folder"], files_regex=params["files_regex"],
                   common_frequency_hz=params["common_frequency_hz"],
                   sample_length=params["sample_length"], split_train=params["split_train"])


class WalkingRobotFromRawDataLoader(Loader):
    CLASSES = 0
    INPUTS = "inputs"
    LABELS = "labels"

    def __init__(self, data_dir: str = "", files_ext: str = "", keys=None, max_signal_length: int = 100,
                 num_legs: int = 4):
        super(WalkingRobotFromRawDataLoader, self).__init__()
        if keys is None:
            keys = {}

        self.data_dir = data_dir
        self.files_ext = files_ext
        self.keys = keys
        self.max_signal_length = max_signal_length
        self.num_legs = num_legs

    @classmethod
    def from_json(cls, json_path: str) -> 'WalkingRobotFromRawDataLoader':
        with open(json_path, 'r') as f:
            params = json.load(f)
        return cls(data_dir=params["data_dir"], files_ext=params["files_ext"], keys=params["keys"],
                   max_signal_length=params["max_signal_length"], num_legs=params["num_legs"])
