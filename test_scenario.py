import os
import sys
from argparse import ArgumentParser

from sklearn.preprocessing import MultiLabelBinarizer

from data import *
from data.utilities import load_bigs_dataset, load_hatt_dataset
from data.utilities.load_functions import load_vt_dataset
from nn import LMF, MidFusion, allow_memory_growth, test, test_multilabel
from nn.instances.LateFusion import LateFusion
from nn.instances.LateSupervisionFusion import LateSupervisionFusion
from nn.utilities.noise_functions import bigs_noises, hatt_noises, bolt_noises

tf.random.set_seed(0)

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"


def test_scenario(args):
    multilabel = False
    class_names = None
    classes_idx = None

    if args.model_type.startswith("hatt"):
        x_train, y_train, x_test, y_test, class_names_full = load_hatt_dataset(args.data_loader_config,
                                                                               args.data_path_train,
                                                                               args.data_path_test,
                                                                               use_force=True,
                                                                               use_accel=True,
                                                                               use_vel=True,
                                                                               normalize=True)
        parse_fun = hatt_img_timeserie_parse_function_aug
        class_names = np.unique(class_names_full)
        num_classes = len(np.unique(y_train))
        noise_functions = hatt_noises

    elif args.model_type.startswith("bigs"):
        x_train, y_train, x_test, y_test, class_names_full = load_bigs_dataset(args.data_loader_config,
                                                                               args.data_path_train,
                                                                               args.data_path_test,
                                                                               use_forces=True,
                                                                               use_prioprioception=True,
                                                                               normalize=True)
        parse_fun = bigs_raw_electrodes_parse_function
        class_names = np.unique(class_names_full)
        num_classes = len(np.unique(y_train))
        noise_functions = bigs_noises

    elif args.model_type.startswith("vt"):
        x_train, y_train, x_test, y_test = load_vt_dataset(args.data_loader_config, args.data_path_train,
                                                           args.data_path_test,
                                                           use_front_0=False, use_front_1=True,
                                                           use_left_0=False, use_left_1=True,
                                                           use_tactile=True,
                                                           normalize=True)
        parse_fun = vt_img_timeserie_parse_function_aug
        num_classes = len(np.unique(y_train))
        noise_functions = None

    elif args.model_type.startswith("bolt"):
        x_train, y_train, x_test, y_test, class_names_full = load_bolt_dataset(args.data_loader_config,
                                                                               args.data_path_train,
                                                                               args.data_path_test, use_images=True,
                                                                               use_electrodes=True,
                                                                               use_pdc=False, use_pac=False,
                                                                               use_tdc=False, use_tac=False,
                                                                               normalize=True)
        parse_fun = bolt_raw_electrodes_with_images_parse_function_aug
        multilabel = True
        class_names = np.unique(np.hstack(class_names_full))
        classes_idx = np.unique(np.hstack(y_train))
        num_classes = len(classes_idx)
        noise_functions = bolt_noises

    else:
        print("No known dataset loaded.")
        sys.exit(1)

    # binarize multilabel data, otherwise use categorical values
    if multilabel:
        mlb = MultiLabelBinarizer()
        mlb.set_params(classes=classes_idx)
        y_test = mlb.fit_transform(y_test)

    # choose a model type
    net_args = [args.batch_size, num_classes, args.model_type]
    if args.model_type.endswith("tensor"):
        model = LMF(*net_args)
    elif args.model_type.endswith("mid"):
        model = MidFusion(*net_args)
    elif args.model_type.endswith("late"):
        model = LateFusion(*net_args)
    elif args.model_type.endswith("late_super"):
        model = LateSupervisionFusion(*net_args)
    else:
        print("No known model loaded.")
        sys.exit(1)

    # setup optimization procedure
    ckpt = tf.train.Checkpoint(model=model)

    # restore from checkpoint
    logs_path = args.results
    path = tf.train.latest_checkpoint(logs_path)
    ckpt.restore(path)
    print("Restored from: {}".format(logs_path))

    # get test data
    data_to_use_test = (*x_test, y_test)
    test_ds = tf.data.Dataset.from_tensor_slices(data_to_use_test) \
        .map(parse_fun, 8) \
        .batch(args.batch_size) \
        .shuffle(x_test[0].shape[0])

    # run a training procedure
    if multilabel:
        test_multilabel(model, test_ds, class_names, noise_functions)
    else:
        test(model, test_ds, class_names, noise_functions)


if __name__ == '__main__':
    available_model_types = ['bigs_tensor', 'bigs_mid', 'bigs_late', 'bigs_late_super',
                             'hatt_tensor', 'hatt_mid', 'hatt_late', 'hatt_late_super',
                             'vt_tensor', 'vt_mid', 'vt_late', 'vt_late_super',
                             'bolt_tensor', 'bolt_mid', 'bolt_late', 'bolt_late_super']

    parser = ArgumentParser()
    parser.add_argument('--model-type', type=str, required=True,
                        choices=available_model_types)

    parser.add_argument('--data-loader-config', type=str, required=True)
    parser.add_argument('--data-path-train', type=str, required=True)
    parser.add_argument('--data-path-test', type=str, required=True)
    parser.add_argument('--results', type=str, default="cross_validation/test")
    parser.add_argument('--batch-size', type=int, default=32)
    parser.add_argument('--latent-vec-size', type=int, default=100)

    args, _ = parser.parse_known_args()

    if args.model_type not in available_model_types:
        parser.print_help()
        sys.exit(1)

    allow_memory_growth()
    test_scenario(args)
