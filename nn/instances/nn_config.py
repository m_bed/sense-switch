mod_dicts = {
    "bigs_tensor": {
        "rank_mf": 1,
        "images": None,
        "signals": {
            "num_modalities": 3,
            "lv_size": 10,
            "conv_filters": [64, 64, 64],
            "conv_kernels": [5, 5, 5],
            "conv_strides": [2, 2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 10],
            "lstm_units": 32,
            "one_direction_only": False,
            "return_sequences": False
        }
    },
    "bigs_mid": {
        "rank_mf": 1,
        "images": None,
        "signals": {
            "num_modalities": 3,
            "lv_size": 10,
            "conv_filters": [64, 64, 64],
            "conv_kernels": [5, 5, 5],
            "conv_strides": [2, 2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 2],
            "lstm_units": 32,
            "one_direction_only": False,
            "return_sequences": False
        }
    },
    "bigs_late": {
        "rank_mf": 1,
        "images": None,
        "signals": {
            "num_modalities": 3,
            "lv_size": 10,
            "conv_filters": [64, 64, 64],
            "conv_kernels": [5, 5, 5],
            "conv_strides": [2, 2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 2],
            "lstm_units": 32,
            "one_direction_only": False,
            "return_sequences": False
        }
    },
    "bigs_late_super": {
        "rank_mf": 1,
        "images": None,
        "signals": {
            "num_modalities": 3,
            "lv_size": 10,
            "conv_filters": [52, 52, 52],
            "conv_kernels": [5, 5, 5],
            "conv_strides": [2, 2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 2],
            "lstm_units": 32,
            "one_direction_only": False,
            "return_sequences": False
        }
    },
    "hatt_tensor": {
        "rank_mf": 1,
        "images": None,
        # "images": {
        #     "num_modalities": 1,
        #     "lv_size": 10,
        #     "conv_filters": [64, 64],
        #     "conv_kernels": [5, 5],
        #     "conv_strides": [2, 2],
        #     "dropout": 0.1,
        #     "fc_layers": [128, 100],
        # },
        "signals": {
            "num_modalities": 3,
            "lv_size": 10,
            "conv_filters": [64, 64, 64],
            "conv_kernels": [5, 5, 5],
            "conv_strides": [2, 2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 10],
            "lstm_units": 32,
            "one_direction_only": False,
            "return_sequences": False
        }
    },
    "hatt_mid": {
        "rank_mf": 1,
        "images": None,
        # "images": {
        #     "num_modalities": 1,
        #     "lv_size": 10,
        #     "conv_filters": [64, 64],
        #     "conv_kernels": [5, 5],
        #     "conv_strides": [2, 2],
        #     "dropout": 0.1,
        #     "fc_layers": [128, 100],
        # },
        "signals": {
            "num_modalities": 3,
            "lv_size": 10,
            "conv_filters": [64, 64, 64],
            "conv_kernels": [5, 5, 5],
            "conv_strides": [2, 2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 10],
            "lstm_units": 32,
            "one_direction_only": False,
            "return_sequences": False
        }
    },
    "hatt_late": {
        "rank_mf": 1,
        "images": None,
        # "images": {
        #     "num_modalities": 1,
        #     "lv_size": 10,
        #     "conv_filters": [64, 64],
        #     "conv_kernels": [5, 5],
        #     "conv_strides": [2, 2],
        #     "dropout": 0.1,
        #     "fc_layers": [128, 100],
        # },
        "signals": {
            "num_modalities": 3,
            "lv_size": 10,
            "conv_filters": [64, 64, 64],
            "conv_kernels": [5, 5, 5],
            "conv_strides": [2, 2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 10],
            "lstm_units": 32,
            "one_direction_only": False,
            "return_sequences": False
        }
    },
    "hatt_late_super": {
        "rank_mf": 1,
        "images": None,
        # "images": {
        #     "num_modalities": 1,
        #     "lv_size": 10,
        #     "conv_filters": [64, 64],
        #     "conv_kernels": [5, 5],
        #     "conv_strides": [2, 2],
        #     "dropout": 0.1,
        #     "fc_layers": [128, 100],
        # },
        "signals": {
            "num_modalities": 3,
            "lv_size": 10,
            "conv_filters": [52, 52, 52],
            "conv_kernels": [5, 5, 5],
            "conv_strides": [2, 2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 10],
            "lstm_units": 32,
            "one_direction_only": False,
            "return_sequences": False
        }
    },
    "vt_tensor": {
        "rank_mf": 1,
        "images": {
            "num_modalities": 2,
            "lv_size": 10,
            "conv_filters": [64, 64],
            "conv_kernels": [5, 5],
            "conv_strides": [2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 10],
        },
        "signals": {
            "num_modalities": 1,
            "lv_size": 10,
            "conv_filters": [64, 64, 64],
            "conv_kernels": [5, 5, 5],
            "conv_strides": [2, 2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 10],
            "lstm_units": 32,
            "one_direction_only": False,
            "return_sequences": False
        }
    },
    "vt_mid": {
        "rank_mf": 1,
        "images": {
            "num_modalities": 2,
            "lv_size": 10,
            "conv_filters": [64, 64],
            "conv_kernels": [5, 5],
            "conv_strides": [2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 10],
        },
        "signals": {
            "num_modalities": 1,
            "lv_size": 10,
            "conv_filters": [64, 64],
            "conv_kernels": [5, 5],
            "conv_strides": [2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 10],
            "lstm_units": 32,
            "one_direction_only": False,
            "return_sequences": False
        }
    },
    "vt_late": {
        "rank_mf": 1,
        "images": {
            "num_modalities": 2,
            "lv_size": 6,
            "conv_filters": [64, 64],
            "conv_kernels": [5, 5],
            "conv_strides": [2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 6],
        },
        "signals": {
            "num_modalities": 1,
            "lv_size": 6,
            "conv_filters": [64, 64],
            "conv_kernels": [5, 5],
            "conv_strides": [2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 6],
            "lstm_units": 32,
            "one_direction_only": False,
            "return_sequences": False
        }
    },
    "vt_late_super": {
        "rank_mf": 1,
        "images": {
            "num_modalities": 2,
            "lv_size": 6,
            "conv_filters": [64, 64],
            "conv_kernels": [5, 5],
            "conv_strides": [2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 6],
        },
        "signals": {
            "num_modalities": 1,
            "lv_size": 6,
            "conv_filters": [64, 64],
            "conv_kernels": [5, 5],
            "conv_strides": [2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 6],
            "lstm_units": 32,
            "one_direction_only": False,
            "return_sequences": False
        }
    },
    "bolt_late": {
        "rank_mf": 1,
        "images": {
            "num_modalities": 1,
            "lv_size": 10,
            "conv_filters": [64, 64],
            "conv_kernels": [5, 5],
            "conv_strides": [2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 24],
        },
        "signals": {
            "num_modalities": 2,
            "lv_size": 10,
            "conv_filters": [64, 64],
            "conv_kernels": [5, 5],
            "conv_strides": [2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 24],
            "lstm_units": 32,
            "one_direction_only": False,
            "return_sequences": False
        }
    },
    "bolt_late_super": {
        "rank_mf": 1,
        "images": {
            "num_modalities": 1,
            "lv_size": 10,
            "conv_filters": [64, 64],
            "conv_kernels": [5, 5],
            "conv_strides": [2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 24],
        },
        "signals": {
            "num_modalities": 2,
            "lv_size": 10,
            "conv_filters": [64, 64],
            "conv_kernels": [5, 5],
            "conv_strides": [2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 24],
            "lstm_units": 32,
            "one_direction_only": False,
            "return_sequences": False
        }
    },
    "bolt_mid": {
        "rank_mf": 1,
        "images": {
            "num_modalities": 1,
            "lv_size": 10,
            "conv_filters": [64, 64],
            "conv_kernels": [5, 5],
            "conv_strides": [2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 24],
        },
        "signals": {
            "num_modalities": 2,
            "lv_size": 10,
            "conv_filters": [64, 64],
            "conv_kernels": [5, 5],
            "conv_strides": [2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 24],
            "lstm_units": 32,
            "one_direction_only": False,
            "return_sequences": False
        }
    },
    "bolt_tensor": {
        "rank_mf": 1,
        "images": {
            "num_modalities": 1,
            "lv_size": 10,
            "conv_filters": [64, 64],
            "conv_kernels": [5, 5],
            "conv_strides": [2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 10],
        },
        "signals": {
            "num_modalities": 2,
            "lv_size": 10,
            "conv_filters": [64, 64, 64],
            "conv_kernels": [5, 5, 5],
            "conv_strides": [2, 2, 2],
            "dropout": 0.1,
            "fc_layers": [128, 10],
            "lstm_units": 32,
            "one_direction_only": False,
            "return_sequences": False
        }
    },
}
