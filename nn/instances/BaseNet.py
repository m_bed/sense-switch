import tensorflow as tf

from .layers import create_bidir_lstm_layer
from .nn_config import mod_dicts


class BaseNet(tf.keras.Model):

    def __init__(self, batch_size: int, num_classes: int, modalities_dict: str):
        super(BaseNet, self).__init__()

        # specify the configuration based on implemented dataset
        if modalities_dict not in ['bigs_tensor', 'bigs_mid', 'bigs_late', 'bigs_late_super',
                                   'hatt_tensor', 'hatt_mid', 'hatt_late', 'hatt_late_super',
                                   'vt_tensor', 'vt_mid', 'vt_late', 'vt_late_super',
                                   'bolt_tensor', 'bolt_mid', 'bolt_late', 'bolt_late_super']:
            print("Unknown dataset. Try again.")

        self.mod_dict = mod_dicts[modalities_dict]
        self.num_classes = num_classes
        self.batch_size = batch_size

        # verify a number of inputs
        self.num_img_nets = 0 if self.mod_dict["images"] is None else self.mod_dict["images"]["num_modalities"]
        self.num_sig_nets = 0 if self.mod_dict["signals"] is None else self.mod_dict["signals"]["num_modalities"]
        self.num_modalities = self.num_img_nets + self.num_sig_nets

        # each sub-net is composed of a tuple of elements, e.g. [convolutional_block, fc_block]
        # create desired number of subnets
        self.image_networks = []
        self.time_series_networks = []

        # setup image nets
        if self.mod_dict["images"] is not None:
            settings = self.mod_dict["images"]
            for _ in range(self.num_img_nets):
                conv = self._conv_blocks(conv_filters=settings["conv_filters"],
                                         conv_kernels=settings["conv_kernels"],
                                         conv_strides=settings["conv_strides"],
                                         dropout=settings["dropout"],
                                         conv_func=tf.keras.layers.Conv2D)

                fc1 = self._fc_blocks(fc_layers=settings["fc_layers"][:1],
                                      dropout=settings["dropout"])
                fc2 = self._fc_blocks(fc_layers=settings["fc_layers"][1:],
                                      dropout=settings["dropout"])

                self.image_networks.append([conv, fc1, fc2])

        # setup time-series nets
        if self.mod_dict["signals"] is not None:
            settings = self.mod_dict["signals"]
            for _ in range(self.num_sig_nets):
                conv = self._conv_blocks(conv_filters=settings["conv_filters"],
                                         conv_kernels=settings["conv_kernels"],
                                         conv_strides=settings["conv_strides"],
                                         dropout=settings["dropout"],
                                         conv_func=tf.keras.layers.Conv1D)

                lstm = self._recurrent_block(lstm_units=settings["lstm_units"],
                                             one_direction_only=settings["one_direction_only"],
                                             return_sequences=settings["return_sequences"],
                                             dropout=settings["dropout"])

                fc1 = self._fc_blocks(fc_layers=settings["fc_layers"][:1],
                                      dropout=settings["dropout"])
                fc2 = self._fc_blocks(fc_layers=settings["fc_layers"][1:],
                                      dropout=settings["dropout"])

                self.time_series_networks.append([conv, lstm, fc1, fc2])

        # print info about the network
        print(
            "Network model initialized.\n"
            "Number of modalities: {}\n"
            "Number of time-series: {}\n"
            "Number of images: {}".format(
                self.num_img_nets + self.num_sig_nets, self.num_sig_nets, self.num_img_nets))

    def call(self, inputs, training=None, mask=None):
        raise NotImplementedError("Please implement that method.")

    def _decode(self, inputs: list, training: bool, net: list):
        latent_list = list()
        partial_results = []
        for i, mod in enumerate(inputs):
            subnets = net[i]
            x = mod
            stages = list()
            for subnet in subnets:
                x = subnet(x, training=training)
                stages.append(x)
            latent_list.append(x)
            partial_results.append(stages)
        return latent_list, partial_results

    def _split_input_data(self, inputs):
        inputs_sig, inputs_img = [], []
        for mod in inputs:
            if len(mod.numpy().shape[1:]) == 2:
                inputs_sig.append(mod)
            else:
                inputs_img.append(mod)
        return inputs_sig, inputs_img

    def _recurrent_block(self, one_direction_only=True, lstm_units=128, dropout=0.0, return_sequences=False):
        if one_direction_only:
            block = tf.keras.layers.LSTM(lstm_units, return_sequences=return_sequences, dtype=tf.float64,
                                         dropout=dropout)
        else:
            block = create_bidir_lstm_layer(self.batch_size, lstm_units, return_sequences=return_sequences,
                                            dropout=dropout)
        return block

    def _fc_blocks(self, fc_layers, dropout):
        fc_net = tf.keras.Sequential()
        fc_net.add(tf.keras.layers.Flatten())
        for i, fc_units in enumerate(fc_layers):
            fc_net.add(tf.keras.layers.Dense(fc_units))

            if i != len(fc_layers) - 1:
                # fc_net.add(tf.keras.layers.BatchNormalization())
                fc_net.add(tf.keras.layers.Activation("relu"))
                # fc_net.add(tf.keras.layers.Dropout(dropout))

        return fc_net

    def _conv_blocks(self, conv_filters, conv_kernels, conv_strides, dropout, conv_func=tf.keras.layers.Conv2D):
        conv_net = tf.keras.Sequential()

        # create conv2d blocks
        for i, (num_filters, kernel, stride) in enumerate(zip(conv_filters, conv_kernels, conv_strides)):
            conv_net.add(conv_func(num_filters, kernel, stride, padding="SAME"))

            if i != len(conv_filters) - 1:
                # conv_net.add(tf.keras.layers.BatchNormalization())
                conv_net.add(tf.keras.layers.Activation("relu"))
                # conv_net.add(tf.keras.layers.Dropout(dropout))
        return conv_net
