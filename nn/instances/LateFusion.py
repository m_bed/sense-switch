import tensorflow as tf

from .BaseNet import BaseNet


class LateFusion(BaseNet):

    def __init__(self, batch_size: int, num_classes: int, modalities_dict: str):
        super(LateFusion, self).__init__(batch_size, num_classes, modalities_dict)

    def call(self, inputs, training=None, mask=None):
        inputs_sig, inputs_img = self._split_input_data(inputs)

        # detect a shape of an input and pass it to the proper sub-network
        lvs_img, _ = self._decode(inputs_img, training, self.image_networks)
        lvs_sig, _ = self._decode(inputs_sig, training, self.time_series_networks)
        lvs = lvs_img + lvs_sig
        x = tf.stack(lvs, axis=-1)
        x = tf.reduce_mean(x, axis=-1)
        return x, None, None
