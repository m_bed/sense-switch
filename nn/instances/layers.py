import tensorflow as tf
from tensorflow.python.ops.array_ops import reverse_sequence


class Bidirectional(tf.keras.Model):
    """
    Custom implementation of bidirectional processing. Default keras implementation does not support situation
    when inputs in batch have different length (it process also padded zeros, what may significantly change
    results). This implementation also processes all inputs (also padded zeros), but then masks the outputs
    with 1 where input exists and 0 otherwise.
    """

    def __init__(self, fw_cell: tf.keras.layers.RNN, bw_cell: tf.keras.layers.RNN = None, merge_mode='concat',
                 return_sequences=True):
        super(Bidirectional, self).__init__()
        self.fw_cell = fw_cell
        self.bw_cell = bw_cell if bw_cell is not None else fw_cell
        self.merge_mode = merge_mode
        self.return_sequences = return_sequences

    def call(self, inputs, training=None, initial_state=None, mask=None):
        sequences, lengths = inputs
        batch_size, max_sequence = tf.shape(sequences)[0], tf.shape(sequences)[1]
        # get input and reversed input
        sequences_rev = reverse_sequence(sequences, lengths, 1, 0)
        # prepare mask according to the sequences lengths
        mask = tf.range(0, max_sequence)[tf.newaxis, :]
        mask = tf.tile(mask, (batch_size, 1)) < lengths[:, tf.newaxis]
        # run both and reverse again results from reversed lstm
        fw_results = self.fw_cell(sequences, training=training, initial_state=initial_state, mask=mask)
        bw_results = self.bw_cell(sequences_rev, training=training, initial_state=initial_state, mask=mask)
        bw_results = reverse_sequence(bw_results, lengths, 1, 0)
        # if not return sequences then replace fw and bw outputs by only the last one and pass to merge stage
        if not self.return_sequences:
            idx_last = tf.stack([tf.range(0, batch_size), lengths - 1], 1)
            idx_first = tf.stack([tf.range(0, batch_size), tf.zeros_like(lengths)], 1)
            fw_results = tf.gather_nd(fw_results, idx_last)
            bw_results = tf.gather_nd(bw_results, idx_first)
        # mask the output and merge it (somehow - concat, add etc)
        if self.merge_mode == 'concat':
            x = tf.concat([fw_results, bw_results], -1)
        elif self.merge_mode == 'sum':
            x = fw_results + bw_results
        elif self.merge_mode == 'ave':
            x = (fw_results + bw_results) / 2
        elif self.merge_mode == 'mul':
            x = fw_results * bw_results
        elif self.merge_mode is None:
            x = [fw_results, bw_results]
        else:
            raise AttributeError('select merge mode')
        return x


def create_bidir_lstm_layer(batch_size, lstm_units, return_sequences=False, dropout=0.0):
    forward_layer = tf.keras.layers.LSTM(lstm_units, return_sequences=return_sequences, dtype=tf.float64,
                                         dropout=dropout)
    backward_layer = tf.keras.layers.LSTM(lstm_units, return_sequences=return_sequences, go_backwards=True,
                                          dropout=dropout, dtype=tf.float64)
    return tf.keras.layers.Bidirectional(forward_layer, backward_layer=backward_layer,
                                         input_shape=(batch_size, int(2 * lstm_units)))


def create_resnet_v2_block_2d(num_filters, kernel_size=3, stride=1, activation_fcn='relu'):
    return tf.keras.Sequential([
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.Activation(activation_fcn),
        tf.keras.layers.Conv2D(num_filters, kernel_size, stride, padding='SAME'),
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.Activation(activation_fcn),
        tf.keras.layers.Conv2D(num_filters, kernel_size, stride, padding='SAME')
    ])


def create_image_network(num_outputs,
                         conv_filters: list = (64, 64),
                         fc_layers: list = (64,),
                         dropout=0.3, stride=3, kernel=5):
    net = tf.keras.Sequential()

    # create conv2d blocks
    for filters in conv_filters:
        net.add(tf.keras.layers.Conv2D(filters, kernel, stride, padding="SAME"))
        net.add(tf.keras.layers.BatchNormalization())
        net.add(tf.keras.layers.Activation("relu"))
        net.add(tf.keras.layers.Dropout(dropout))

    # create output layer
    net.add(tf.keras.layers.Flatten())
    for fc_units in fc_layers:
        net.add(tf.keras.layers.Dense(fc_units))
        net.add(tf.keras.layers.BatchNormalization())
        net.add(tf.keras.layers.Activation("relu"))
        net.add(tf.keras.layers.Dropout(dropout))

    # add number of outputs
    if num_outputs is not None:
        net.add(tf.keras.layers.Dense(num_outputs, None))
    return net


def create_signal_network(batch_size, num_outputs,
                          conv_filters: list = (64, 64),
                          conv_kernels: list = (3, 3),
                          conv_strides: list = (2, 2),
                          bilstm_units: list = (64,),
                          fc_layers: list = (64,)):
    assert len(conv_strides) == len(conv_kernels) == len(conv_filters)

    # create conv1d blocks
    conv_net = tf.keras.Sequential()
    for i, (num_filters, kernel, stride) in enumerate(zip(conv_filters, conv_kernels, conv_strides)):
        conv_net.add(tf.keras.layers.Conv1D(num_filters, kernel, stride, padding="SAME"))

        if i != len(conv_filters) - 1:
            conv_net.add(tf.keras.layers.BatchNormalization())
            conv_net.add(tf.keras.layers.Activation("relu"))

    # create bilstm modules
    lstm_net = tf.keras.Sequential()
    for i, unit_size in enumerate(bilstm_units):
        return_sequences = True
        if i == len(bilstm_units) - 1:
            return_sequences = False
        lstm_net.add(create_bidir_lstm_layer(batch_size, unit_size, return_sequences=return_sequences))

    # create output layer
    fc_net = tf.keras.Sequential()
    fc_net.add(tf.keras.layers.Flatten())
    for i, fc_units in enumerate(fc_layers):
        fc_net.add(tf.keras.layers.Dense(fc_units))

        if i != len(fc_layers) - 1:
            fc_net.add(tf.keras.layers.BatchNormalization())
            fc_net.add(tf.keras.layers.Activation("relu"))

    # add number of outputs
    if num_outputs is not None and num_outputs >= 1:
        fc_net.add(tf.keras.layers.Dense(num_outputs, None))

    return conv_net, lstm_net, fc_net


def create_fusion_layer(num_outputs, fc_layers: list = (128, 64), dropout=0.3):
    net = tf.keras.Sequential()

    net.add(tf.keras.layers.Flatten())
    for fc_units in fc_layers:
        net.add(tf.keras.layers.Dense(fc_units))
        net.add(tf.keras.layers.BatchNormalization())
        net.add(tf.keras.layers.Activation("relu"))
        net.add(tf.keras.layers.Dropout(dropout))

    # add number of outputs
    if num_outputs is not None:
        net.add(tf.keras.layers.Dense(num_outputs, None))
    return net


class Fusion_layer_1D(tf.keras.layers.Layer):
    def __init__(self, num_modalities: int):
        if num_modalities > 3:
            raise ValueError("Unsupported number of modalities.")

        super().__init__(True, "TensorFusionLayer", tf.float32)
        self.num_modalities = num_modalities

    def call(self, inputs, **kwargs):

        num_modalities = len(inputs)

        # Low-rank Multimodal Fusion
        b = tf.shape(inputs[0]).numpy()[0]
        ones = tf.ones(shape=(b, 1), dtype=inputs[0].dtype)
        inputs = [tf.concat([ones, lv], -1) for lv in inputs]
        tensor = self.intermediate_representation(inputs, num_modalities, from_list=True)

        # for three modalities generate heatmaps also
        heatmaps = None
        if len(inputs) > 2:
            heatmaps = self._generate_bimodal_correlation_maps(inputs, num_modalities)

        return tensor, heatmaps

    def _generate_aut_einsum_expr(self, operation_rank, start_with=99):
        end_char = start_with + operation_rank
        generated_string = "".join([chr(i) for i in range(start_with, end_char)])
        return generated_string

    def intermediate_representation(self, inputs, num_modalities, from_list=False):
        x = inputs[0] if from_list else inputs
        for _ in range(1, num_modalities):
            next_val = inputs[0] if from_list else inputs

            r1 = tf.rank(x).numpy() - 1
            r2 = tf.rank(next_val).numpy() - 1
            expr1 = self._generate_aut_einsum_expr(r1)
            expr2 = self._generate_aut_einsum_expr(r2, start_with=99 + len(expr1))
            einsum_str = "b{},b{}->b{}".format(expr1, expr2, expr1 + expr2)

            x = tf.einsum(einsum_str, x, next_val)

        return x

    def _generate_bimodal_correlation_maps(self, inputs, num_modalities):
        maps = []
        for i in range(num_modalities):
            for j in range(num_modalities):
                if i != j and j > i:
                    x = tf.einsum("bi,bj->bij", inputs[i], inputs[j])
                    maps.append(x)
        return maps
