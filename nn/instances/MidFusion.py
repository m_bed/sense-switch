import tensorflow as tf

from .BaseNet import BaseNet


class MidFusion(BaseNet):

    def __init__(self, batch_size: int, num_classes: int, modalities_dict: str):
        super(MidFusion, self).__init__(batch_size, num_classes, modalities_dict)
        self.fc = self._fc_blocks(fc_layers=[128, 64, self.num_classes], dropout=0.1)

    def call(self, inputs, training=None, mask=None):
        inputs_sig, inputs_img = self._split_input_data(inputs)

        # detect a shape of an input and pass it to the proper sub-network
        lvs_img, _ = self._decode(inputs_img, training, self.image_networks)
        lvs_sig, _ = self._decode(inputs_sig, training, self.time_series_networks)
        lvs = lvs_img + lvs_sig

        # concatenate latent representation
        latent_vector = tf.concat([*lvs], -1)
        x = self.fc(latent_vector, training=training)

        return x, None, None
