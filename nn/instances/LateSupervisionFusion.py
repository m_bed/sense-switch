import tensorflow as tf

from .BaseNet import BaseNet


class LateSupervisionFusion(BaseNet):

    def __init__(self, batch_size: int, num_classes: int, modalities_dict: str):
        super(LateSupervisionFusion, self).__init__(batch_size, num_classes, modalities_dict)
        self.fc = self._fc_blocks(fc_layers=[128, 64, self.num_modalities], dropout=0.1)

    def call(self, inputs, training=None, mask=None):
        inputs_sig, inputs_img = self._split_input_data(inputs)

        # detect a shape of an input and pass it to the proper sub-network
        lvs_img, partial_results_img = self._decode(inputs_img, training, self.image_networks)
        lvs_sig, partial_results_sig = self._decode(inputs_sig, training, self.time_series_networks)
        partial_results_img = [x[-2] for x in partial_results_img]
        partial_results_sig = [x[-2] for x in partial_results_sig]
        partial_results = partial_results_img + partial_results_sig
        partial_results = tf.concat(partial_results, axis=-1)
        multipliers = self.fc(partial_results)
        multipliers = tf.linalg.normalize(multipliers, axis=-1)[0]
        lvs = lvs_img + lvs_sig
        x = tf.stack(lvs, axis=-1)
        x = x * multipliers[:, tf.newaxis]
        x = tf.reduce_mean(x, axis=-1)
        return x, None, None
