import tensorflow as tf
import tensorly as tl
from tensorly.decomposition import parafac

from .BaseNet import BaseNet
from .layers import Fusion_layer_1D

tl.set_backend('tensorflow')


class LMF(BaseNet):

    def __init__(self, batch_size: int, num_classes: int, modalities_dict: str):
        super(LMF, self).__init__(batch_size, num_classes, modalities_dict)

        self.fusion_layer = Fusion_layer_1D(self.num_modalities)
        self.out_dim = num_classes
        self.rank_mf = self.mod_dict["rank_mf"]

        # create LMF variables
        self.vars_img = self._create_variables(rank=self.rank_mf, settings=self.mod_dict["images"],
                                               weights_name="images",
                                               num_mods=self.num_img_nets)
        self.vars_sig = self._create_variables(rank=self.rank_mf, settings=self.mod_dict["signals"],
                                               weights_name="signals",
                                               num_mods=self.num_sig_nets)

        # create weights for a final layer
        self.fusion_weights = self.add_weight("Fusion_weights",
                                              shape=[1, self.rank_mf],
                                              dtype=tf.float32, initializer=tf.keras.initializers.glorot_normal())
        self.fusion_bias = self.add_weight("FusionBias",
                                           shape=[1, self.out_dim],
                                           dtype=tf.float32, initializer=tf.keras.initializers.glorot_normal())

    def call(self, inputs, training=None, mask=None):
        inputs_sig, inputs_img = self._split_input_data(inputs)

        # detect a shape of an input and pass it to the proper sub-network
        lvs_img, _ = self._decode(inputs_img, training, self.image_networks)
        lvs_sig, _ = self._decode(inputs_sig, training, self.time_series_networks)

        # add ones to capture bimodal and trimodal correlations
        b = tf.shape(inputs[0]).numpy()[0]
        ones = tf.ones(shape=(b, 1), dtype=inputs[0].dtype)
        lvs_img = [tf.concat([ones, lv], -1) for lv in lvs_img]
        lvs_sig = [tf.concat([ones, lv], -1) for lv in lvs_sig]

        # compute the weighted tensor representation
        lvs_img = [tf.matmul(lv, w) for lv, w in zip(lvs_img, self.vars_img)]
        lvs_sig = [tf.matmul(lv, w) for lv, w in zip(lvs_sig, self.vars_sig)]
        lvs = lvs_img + lvs_sig

        # fuse modalities
        x = lvs[0]
        for i in range(1, len(lvs)):
            x = tf.multiply(x, lvs[i])

        # generate output
        output = tf.matmul(self.fusion_weights, tf.transpose(x, (1, 0, 2)))
        output = tf.reshape(output, [b, -1]) + self.fusion_bias

        return output, None, None

    def _create_variables(self, rank, settings, weights_name, num_mods):
        vars = []
        if settings is not None:
            for _ in range(num_mods):
                v = self.add_weight("TensorWeights_{}".format(weights_name),
                                    shape=[rank, settings["lv_size"] + 1, self.out_dim],
                                    dtype=tf.float32, initializer=tf.keras.initializers.glorot_normal())
                vars.append(v)
        return vars

    def _decompose(self, tensor):
        decomposed = tl.tensor(tensor)
        weights, factors = parafac(decomposed, rank=1)

        # reconstruct batch-modality tensors
        f = []
        for i in range(1, len(factors)):
            f.append(tf.einsum("uj,ij->uij", factors[0], factors[i]))

        return tf.squeeze(tf.concat([*f], 1), -1)
