import time

import tensorflow as tf
from tqdm import tqdm

from nn.utilities.optimization import check_best_metric, reset_metrics
from nn.utilities.tensorboard import add_to_tensorboard


def train(model, writer, ds, optimizer, previous_steps, class_names, prefix="train",
          main_mod=None, noise_fun=None):

    metric_loss = tf.keras.metrics.Mean(name="MeanLoss")
    metric_acc = tf.keras.metrics.SparseCategoricalAccuracy(name="SparseCategoricalAccuracy")

    print("\n\n### TRAINING ###\n\n")
    for x_train, y_train in tqdm(ds):

        # add noise to the specified modality
        if main_mod not in [-1, None] and noise_fun is not None:
            x_train = noise_fun(x_train, main_mod)

        with tf.GradientTape() as tape:
            y, full_tensor, heatmaps = model(x_train, training=True)

            loss = tf.reduce_mean(
                tf.keras.losses.sparse_categorical_crossentropy(y_true=y_train,
                                                                y_pred=y,
                                                                from_logits=True)
            )
            metric_loss.update_state(loss.numpy())

            # add regularization
            vars = model.trainable_variables
            l2_reg = tf.add_n([tf.nn.l2_loss(tf.cast(v, tf.float32)) for v in vars]) * 0.001
            loss += l2_reg

        gradients = tape.gradient(loss, model.trainable_variables)
        optimizer.apply_gradients(zip(gradients, model.trainable_variables))

        metric_loss.update_state(loss)
        metric_acc.update_state(y_train, y)

        with writer.as_default():
            add_to_tensorboard({
                "metrics": [metric_acc, metric_loss],
                "full_tensor": None if full_tensor is None else full_tensor[0],
                "heatmaps": None if heatmaps is None else [h[0] for h in heatmaps]
            }, previous_steps, prefix)
            writer.flush()

        previous_steps += 1
        reset_metrics([metric_loss, metric_acc])

    return previous_steps


def validate(model, writer=None, ds=None, previous_steps=None, best_metric=None, class_names=None,
             prefix="validation", is_print=False, noise_fun=None):
    metric_loss = tf.keras.metrics.Mean("MeanLoss")
    metric_acc = tf.keras.metrics.SparseCategoricalAccuracy("SparseCategoricalAccuracy")

    print("\n\n### VALIDATION ###\n\n")
    for x_val, y_val in tqdm(ds):
        y, _, _ = model(x_val, training=False)
        loss = tf.reduce_mean(
            tf.keras.losses.sparse_categorical_crossentropy(y_val, y, from_logits=True)
        )

        metric_loss.update_state(loss)
        metric_acc.update_state(y_val, y)

    # add values to the tensorboard
    if writer is not None:
        with writer.as_default():
            add_to_tensorboard({
                "metrics": [metric_acc, metric_loss]
            }, previous_steps, prefix)
            writer.flush()
        previous_steps += 1

    # verify the best metric and reset metrics
    save_model, best_metric = check_best_metric(metric_acc, best_metric, is_print, previous_steps)
    reset_metrics([metric_loss, metric_acc])

    return previous_steps, save_model, best_metric


def test(model, ds, class_names, noise_functions):
    metric_loss = tf.keras.metrics.Mean("MeanLoss")
    metric_time = tf.keras.metrics.Mean("MeanTime")
    metric_acc = tf.keras.metrics.SparseCategoricalAccuracy("SparseCategoricalAccuracy")

    for noise_key in noise_functions:

        for x_val, y_val in tqdm(ds):
            if noise_functions[noise_key] is not None:
                func = noise_functions[noise_key][0]
                noised_modalities = noise_functions[noise_key][1]
                x_val = func(x_val, noised_modalities)

            t = time.process_time()
            y, _, _ = model(x_val, training=False)
            elapsed_time = time.process_time() - t

            loss = tf.reduce_mean(tf.keras.losses.sparse_categorical_crossentropy(y_val, y, from_logits=True))
            metric_loss.update_state(loss)
            metric_time.update_state(elapsed_time)
            metric_acc.update_state(y_val, y)

        print("\n\n### TEST FOR NOISE {} ###".format(noise_key))
        print("MEAN ACCURACY: {}".format(metric_acc.result().numpy()))
        print("MEAN LOSS: {}".format(metric_loss.result().numpy()))
        print("MEAN INFERENCE TIME: {}".format(metric_time.result().numpy()))
        reset_metrics([metric_loss, metric_time, metric_acc])
