import numpy as np
import tensorflow as tf

MINVAL, MAXVAL = 0.5, 0.5
MEAN, STDDEV = 0.0, 0.7


def random_noise(x, main_mod_idx, mean=MEAN, stddev=STDDEV, fraction_noised=0.33):
    x = list(x)
    modality_samples = x[main_mod_idx]
    shape = tf.shape(modality_samples).numpy()
    batch_size = shape[0]
    random_batch_idxs = np.random.choice(batch_size, int(fraction_noised * batch_size))
    modified_batch = list()

    for idx in range(batch_size):
        sample = modality_samples[idx, ...]
        if idx in random_batch_idxs:
            if np.random.rand() > 0.5:
                sample = tf.zeros(shape[1:], dtype=sample.dtype)
            else:
                sample = sample + tf.random.normal(shape[1:], mean, stddev, dtype=sample.dtype)
        modified_batch.append(sample)

    x[main_mod_idx] = tf.convert_to_tensor(modified_batch)
    return x


def add_normal_noise(x, noised_modalities, mean=MEAN, stddev=STDDEV):
    x = list(x)
    for i in range(len(x)):
        if i in noised_modalities:
            shape = tf.shape(x[i]).numpy()
            noise = tf.random.normal(shape, mean, stddev, dtype=x[i].dtype)
            x[i] += noise
    return x


def add_uniform_noise(x, noised_modalities, minval=MINVAL, maxval=MAXVAL):
    x = list(x)
    for i in range(len(x)):
        if i in noised_modalities:
            shape = tf.shape(x[i]).numpy()
            noise = tf.random.uniform(shape, minval, maxval, dtype=x[i].dtype)
            x[i] += noise
    return x


def add_switchoff(x, noised_modalities):
    x = list(x)
    for i in range(len(x)):
        if i in noised_modalities:
            shape = tf.shape(x[i]).numpy()
            noise = tf.zeros(shape, dtype=x[i].dtype)
            x[i] = noise
    return x


def replace_with_normal_noise(x, noised_modalities):
    x = list(x)
    for i in range(len(x)):
        if i in noised_modalities:
            shape = tf.shape(x[i]).numpy()
            noise = tf.random.normal(shape, MEAN, STDDEV, dtype=x[i].dtype)
            x[i] = noise
    return x


def replace_with_uniform_noise(x, noised_modalities):
    x = list(x)
    for i in range(len(x)):
        if i in noised_modalities:
            shape = tf.shape(x[i]).numpy()
            noise = tf.random.uniform(shape, MINVAL, MAXVAL, dtype=x[i].dtype)
            x[i] = noise
    return x


# list of noises added to input data for each dataset experiment
bigs_noises = {
    None: None,
    # "normal_1": [add_normal_noise, [0]],
    # "normal_2": [add_normal_noise, [1]],
    "normal_3": [add_normal_noise, [2]],
    # "normal_1_2": [add_normal_noise, [0, 1]],
    # "normal_1_3": [add_normal_noise, [0, 2]],
    # "normal_2_3": [add_normal_noise, [1, 2]],
    # "uniform_1": [add_uniform_noise, [0]],
    # "uniform_2": [add_uniform_noise, [1]],
    # "uniform_3": [add_uniform_noise, [2]],
    # "uniform_1_2": [add_uniform_noise, [0, 1]],
    # "uniform_1_3": [add_uniform_noise, [0, 2]],
    # "uniform_2_3": [add_uniform_noise, [1, 2]],
    # "blank_1": [add_switchoff, [0]],
    # "blank_2": [add_switchoff, [1]],
    "blank_3": [add_switchoff, [2]],
    # "blank_1_2": [add_switchoff, [0, 1]],
    # "blank_1_3": [add_switchoff, [0, 2]],
    # "blank_2_3": [add_switchoff, [1, 2]],
    # "replace_normal_1": [replace_with_normal_noise, [0]],
    # "replace_normal_2": [replace_with_normal_noise, [1]],
    # "replace_normal_3": [replace_with_normal_noise, [2]],
    # "replace_normal_1_2": [replace_with_normal_noise, [0, 1]],
    # "replace_normal_1_3": [replace_with_normal_noise, [0, 2]],
    # "replace_normal_2_3": [replace_with_normal_noise, [1, 2]],
    # "replace_uniform_1": [replace_with_uniform_noise, [0]],
    # "replace_uniform_2": [replace_with_uniform_noise, [1]],
    # "replace_uniform_3": [replace_with_uniform_noise, [2]],
    # "replace_uniform_1_2": [replace_with_uniform_noise, [0, 1]],
    # "replace_uniform_1_3": [replace_with_uniform_noise, [0, 2]],
    # "replace_uniform_2_3": [replace_with_uniform_noise, [1, 2]],
}

hatt_noises = {
    None: None,
    # "normal_1": [add_normal_noise, [0]],
    "normal_2": [add_normal_noise, [1]],
    # "normal_3": [add_normal_noise, [2]],
    # "normal_1_2": [add_normal_noise, [0, 1]],
    # "normal_1_3": [add_normal_noise, [0, 2]],
    # "normal_2_3": [add_normal_noise, [1, 2]],
    # "uniform_1": [add_uniform_noise, [0]],
    # "uniform_2": [add_uniform_noise, [1]],
    # "uniform_3": [add_uniform_noise, [2]],
    # "uniform_1_2": [add_uniform_noise, [0, 1]],
    # "uniform_1_3": [add_uniform_noise, [0, 2]],
    # "uniform_2_3": [add_uniform_noise, [1, 2]],
    # "blank_1": [add_switchoff, [0]],
    "blank_2": [add_switchoff, [1]],
    # "blank_3": [add_switchoff, [2]],
    # "blank_1_2": [add_switchoff, [0, 1]],
    # "blank_1_3": [add_switchoff, [0, 2]],
    # "blank_2_3": [add_switchoff, [1, 2]],
    # "replace_normal_1": [replace_with_normal_noise, [0]],
    # "replace_normal_2": [replace_with_normal_noise, [1]],
    # "replace_normal_3": [replace_with_normal_noise, [2]],
    # "replace_normal_1_2": [replace_with_normal_noise, [0, 1]],
    # "replace_normal_1_3": [replace_with_normal_noise, [0, 2]],
    # "replace_normal_2_3": [replace_with_normal_noise, [1, 2]],
    # "replace_uniform_1": [replace_with_uniform_noise, [0]],
    # "replace_uniform_2": [replace_with_uniform_noise, [1]],
    # "replace_uniform_3": [replace_with_uniform_noise, [2]],
    # "replace_uniform_1_2": [replace_with_uniform_noise, [0, 1]],
    # "replace_uniform_1_3": [replace_with_uniform_noise, [0, 2]],
    # "replace_uniform_2_3": [replace_with_uniform_noise, [1, 2]],
}

bolt_noises = {
    None: None,
    "normal_1": [add_normal_noise, [0]],
    # "normal_2": [add_normal_noise, [1]],
    # "normal_3": [add_normal_noise, [2]],
    # "normal_1_2": [add_normal_noise, [0, 1]],
    # "normal_1_3": [add_normal_noise, [0, 2]],
    # "normal_2_3": [add_normal_noise, [1, 2]],
    # "uniform_1": [add_uniform_noise, [0]],
    # "uniform_2": [add_uniform_noise, [1]],
    # "uniform_3": [add_uniform_noise, [2]],
    # "uniform_1_2": [add_uniform_noise, [0, 1]],
    # "uniform_1_3": [add_uniform_noise, [0, 2]],
    # "uniform_2_3": [add_uniform_noise, [1, 2]],
    "blank_1": [add_switchoff, [0]],
    # "blank_2": [add_switchoff, [1]],
    # "blank_3": [add_switchoff, [2]],
    # "blank_1_2": [add_switchoff, [0, 1]],
    # "blank_1_3": [add_switchoff, [0, 2]],
    # "blank_2_3": [add_switchoff, [1, 2]],
    # "replace_normal_1": [replace_with_normal_noise, [0]],
    # "replace_normal_2": [replace_with_normal_noise, [1]],
    # "replace_normal_3": [replace_with_normal_noise, [2]],
    # "replace_normal_1_2": [replace_with_normal_noise, [0, 1]],
    # "replace_normal_1_3": [replace_with_normal_noise, [0, 2]],
    # "replace_normal_2_3": [replace_with_normal_noise, [1, 2]],
    # "replace_uniform_1": [replace_with_uniform_noise, [0]],
    # "replace_uniform_2": [replace_with_uniform_noise, [1]],
    # "replace_uniform_3": [replace_with_uniform_noise, [2]],
    # "replace_uniform_1_2": [replace_with_uniform_noise, [0, 1]],
    # "replace_uniform_1_3": [replace_with_uniform_noise, [0, 2]],
    # "replace_uniform_2_3": [replace_with_uniform_noise, [1, 2]],
}

