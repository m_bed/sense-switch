import tensorflow as tf
import tensorflow_probability as tfp

const = 2 * 3.1415


def gauss_function(x, u, stddev):
    x = tf.cast(x, dtype=tf.float32)
    u = tf.cast(u, dtype=tf.float32)
    stddev = tf.cast(stddev, dtype=tf.float32)
    stddev = stddev + 1e-6
    return (1 / tf.sqrt(stddev) * tf.sqrt(const)) * tf.exp(-0.5 * tf.pow((x - u) / stddev, 2.))


def density_to_prob(x, u, stddev):
    dist = tfp.distributions.Normal(u, stddev)
    return dist.prob(x)


def vanishing_modality(data, signal_idx=1, mag=0.5):
    x_val = list(data)
    x_val[signal_idx] *= mag
    return x_val


def noised_modality(data, signal_idx: list = (0, 1), mag: float = 2):
    x_val = list(data)
    for i in signal_idx:
        noise = tf.random.uniform(minval=-mag, maxval=mag, shape=x_val[i].get_shape(),
                                  dtype=x_val[i].dtype)
        x_val[i] += noise
    return x_val
