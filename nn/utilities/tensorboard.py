import io

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d, Axes3D
import numpy as np
import seaborn as sns
import tensorflow as tf

plt.ioff()


def plot3d(data3d, size):
    dx, dy, dz = np.linspace(0, size, size), np.linspace(0, size, size), np.linspace(0, size, size)
    xx, yy = np.meshgrid(dx, dy)
    xx, yy = np.tile(xx[..., np.newaxis], size), np.tile(yy[..., np.newaxis], size)

    xx = xx.flatten()
    yy = yy.flatten()
    zz = np.tile(np.tile(dz[np.newaxis, ...], size), size).flatten()

    # bound colors to the sigmoid
    colors = data3d.numpy().flatten().astype(np.float32)

    fig = plt.figure(figsize=(12, 8))
    ax = fig.add_subplot(111, projection='3d')
    scatter = ax.scatter(xx, yy, zz, s=50, alpha=0.1, edgecolors='w', c=colors, cmap='RdYlBu',
                         vmin=0, vmax=1)

    plt.colorbar(scatter)
    plt.xlabel('X')
    plt.ylabel('Y')
    plt.title('Z')

    return fig


def plot2d(data):
    f, ax = plt.subplots(figsize=(5, 5))
    sns.heatmap(data, annot=False, ax=ax, vmin=0, vmax=1)
    return f


def convert_figure_to_img(figure):
    figure.canvas.draw()
    img = np.array(figure.canvas.renderer.buffer_rgba())[:, :, :3]
    return img[np.newaxis, ...]


def plot_to_image(figure):
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    plt.close(figure)
    buf.seek(0)
    image = tf.image.decode_png(buf.getvalue(), channels=4)
    image = tf.expand_dims(image, 0)
    return image


def add_to_tensorboard(metrics: dict, step, prefix):
    for key in metrics:
        if metrics[key] is None:
            return

        if key in ["metrics", "metrics_per_class", "metrics_auc"]:
            for m in metrics[key]:
                tf.summary.scalar('{}/{}'.format(prefix, m.name), m.result().numpy(), step=step)

        if key is "image":
            img = metrics[key][0]
            minimum = tf.reduce_min(img, (0, 1), True)
            maximum = tf.reduce_max(img, (0, 1), True)
            img = (img - minimum) / (maximum - minimum)
            img = tf.expand_dims(img, 0)
            tf.summary.image('image/{}/{}'.format(prefix, key), img, step=step)

        if key is "full_tensor":

            shape = tf.shape(metrics[key]).numpy()

            if len(shape) > 2:
                size = shape[-1]
                fig = plot3d(metrics[key], size)
                img = plot_to_image(fig)
                plt.close('all')
                tf.summary.image('representation/{}_{}'.format(prefix, key), img, step=step)

            else:
                fig = plot2d(metrics[key])
                img = plot_to_image(fig)
                plt.close('all')
                tf.summary.image('representation/{}_{}'.format(prefix, key), img, step=step)

        if key is "heatmaps":
            if len(metrics[key]) == 0:
                return

            for i, h in enumerate(metrics[key]):
                fig = plot2d(h)
                img = plot_to_image(fig)
                plt.close('all')
                tf.summary.image('representation/{}_{}_{}'.format(prefix, key, i), img, step=step)

