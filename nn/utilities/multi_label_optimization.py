import time

import tensorflow as tf
from tqdm import tqdm

from nn.utilities.optimization import \
    update_metrics, reset_metrics, \
    create_confusion_metrics, create_auc_metrics, check_best_metric
from nn.utilities.tensorboard import add_to_tensorboard


def train_multilabel(model, writer, ds, optimizer, previous_steps, class_names, prefix="train",
                     main_mod=None, noise_fun=None):

    metric_loss = tf.keras.metrics.Mean(name="MeanLoss")
    metric_auc = tf.keras.metrics.Mean(name="MeanAUC")
    confusion_metrics = create_confusion_metrics(model.num_classes, top_k=5)
    auc_metrics = create_auc_metrics(class_names)

    print("\n\n### TRAINING ###\n\n")
    for x_train, y_train in tqdm(ds):

        # add noise to the specified modality
        if main_mod not in [-1, None] and noise_fun is not None:
            x_train = noise_fun(x_train, main_mod)

        with tf.GradientTape() as tape:
            y, full_tensor, heatmaps = model(x_train, training=True)

            # compute loss
            loss = tf.reduce_mean(
                tf.keras.losses.hinge(y_true=y_train, y_pred=y)
            )
            metric_loss.update_state(loss.numpy())

            # add regularization
            vars = model.trainable_variables
            l2_reg = tf.add_n([tf.nn.l2_loss(tf.cast(v, tf.float32)) for v in vars]) * 0.001
            loss += l2_reg

        # apply gradients
        gradients = tape.gradient(loss, model.trainable_variables)
        optimizer.apply_gradients(zip(gradients, model.trainable_variables))

        # update metrics (AUC is a computed separately per each class and mean is a mean from all)
        for i in range(model.num_classes):
            _, auc_metrics_values = update_metrics(y[:, i], y_train[:, i], [auc_metrics[i]], 0.5)
            metric_auc.update_state(auc_metrics_values)
        update_metrics(y, y_train, confusion_metrics, 0.5)

        # add values to the tensorboard
        with writer.as_default():
            add_to_tensorboard({
                # "image": x_train[0],
                "metrics": [metric_loss, *confusion_metrics, *auc_metrics, metric_auc],
                "full_tensor": None if full_tensor is None else full_tensor[0],
                "heatmaps": None if heatmaps is None else [h[0] for h in heatmaps]
            }, previous_steps, prefix)
            writer.flush()

        previous_steps += 1
        metric_loss.reset_states()
        reset_metrics(confusion_metrics + auc_metrics)

    return previous_steps


def validate_multilabel(model, writer=None, ds=None, previous_steps=None, best_metric=None, class_names=None,
                        prefix="validation", is_print=False):
    metric_loss = tf.keras.metrics.Mean(name="MeanLoss")
    metric_auc = tf.keras.metrics.Mean(name="MeanAUC")
    confusion_metrics = create_confusion_metrics(model.num_classes, top_k=5)
    auc_metrics = create_auc_metrics(class_names)

    print("\n\n### VALIDATION ###\n\n")
    for x_val, y_val in tqdm(ds):

        # compute loss
        y, _, _ = model(x_val, training=False)
        loss = tf.reduce_mean(
            tf.keras.losses.hinge(y_true=y_val, y_pred=y)
        )

        # update metrics (AUC is a computed separately per each class and mean is a mean from all)
        metric_loss.update_state(loss)
        for i in range(model.num_classes):
            _, auc_metrics_values = update_metrics(y[:, i], y_val[:, i], [auc_metrics[i]], 0.5)
            metric_auc.update_state(auc_metrics_values)
        update_metrics(y, y_val, confusion_metrics, 0.5)

    # add values to the tensorboard
    if writer is not None:
        with writer.as_default():
            add_to_tensorboard({
                "metrics": [metric_loss, *confusion_metrics, *auc_metrics, metric_auc],
            }, previous_steps, prefix)
            writer.flush()
        previous_steps += 1

    # verify the best metric and reset metrics
    save_model, best_metric = check_best_metric(metric_auc, best_metric, is_print, previous_steps, auc_metrics)
    metric_loss.reset_states()
    reset_metrics(confusion_metrics + auc_metrics)

    return previous_steps, save_model, best_metric


def test_multilabel(model, ds, class_names, noise_functions):
    metric_loss = tf.keras.metrics.Mean(name="MeanLoss")
    metric_auc = tf.keras.metrics.Mean(name="MeanAUC")
    confusion_metrics = create_confusion_metrics(model.num_classes, top_k=5)
    auc_metrics = create_auc_metrics(class_names)
    metric_time = tf.keras.metrics.Mean("MeanTime")

    for noise_key in noise_functions:
        for x_val, y_val in tqdm(ds):

            if noise_functions[noise_key] is not None:
                func = noise_functions[noise_key][0]
                noised_modalities = noise_functions[noise_key][1]
                x_val = func(x_val, noised_modalities)

            # compute loss
            t = time.process_time()
            y, _, _ = model(x_val, training=False)
            elapsed_time = time.process_time() - t

            # update metrics (AUC is a computed separately per each class and mean is a mean from all)
            loss = tf.reduce_mean(tf.keras.losses.hinge(y_true=y_val, y_pred=y))
            metric_loss.update_state(loss)
            metric_time.update_state(elapsed_time)
            for i in range(model.num_classes):
                _, auc_metrics_values = update_metrics(y[:, i], y_val[:, i], [auc_metrics[i]], 0.5)
                metric_auc.update_state(auc_metrics_values)
            update_metrics(y, y_val, confusion_metrics, 0.5)

        print("\n\n### TEST FOR NOISE {} ###".format(noise_key))
        print("MEAN LOSS: {}".format(metric_loss.result().numpy()))
        print("MEAN AUC: {}".format(metric_auc.result().numpy()))
        print("MEAN INFERENCE TIME: {}".format(metric_time.result().numpy()))
        # [print("{}: {}".format(m.name, m.result().numpy())) for m in auc_metrics]
        # [print("{}: {}".format(m.name, m.result().numpy())) for m in confusion_metrics]
        reset_metrics([metric_loss, metric_auc, metric_time, *auc_metrics, *confusion_metrics])
