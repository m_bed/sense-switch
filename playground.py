# BOLT dataset example
# BOLT DATA: https://drive.google.com/open?id=1QpKrCysY9mrRIbFmbUCzQjAeDwWrrDpY
# Extract it to the datasets folder

# There are 424 images in the full dataset
# There are 53 different objects (8 photos for each object)
# Each object has:
# 2x raw electrode values 531x19
# 2x PAC 531x22
# 2x PAD 531x1
# 2x TAC 531x1
# 2x TAD 531x1

import pickle
import random
import PIL
import matplotlib.pyplot as plt
import numpy as np

#  PICK DATASET YOU WANT TO VERIFY
with open("datasets/bolt/full_dataset_53_classes.pickle", "rb") as fp:
    dataset = pickle.load(fp)

# number of unique adjectives
all_adjectives_names = np.hstack(dataset["labels_str"])
unique_adjectives_names, occurances = np.unique(all_adjectives_names, return_counts=True)
print("Number of unique adjectives: {}".format(unique_adjectives_names.shape[0]))
[print("{}: {}".format(name, occ)) for name, occ in zip(unique_adjectives_names, occurances)]

########################## SPLIT DATASET INTO TRAIN 70 / TEST 30
TRAIN_SPLIT = 0.6
paths = dataset["images_paths"]
paths_repetitive_parts = [p[:-14] for p in paths]
_, num_objects = np.unique(paths_repetitive_parts, return_counts=True)


# group dataset photos and signals by objects
def make_group(iterable, size):
    return [iterable[i * step: i * step + step] for i, step in enumerate(size)]


for key in dataset.keys():
    dataset[key] = make_group(dataset[key], num_objects)

# check if each object is represented by equal number of instances in the dataset and pick indexes for split
assert np.mean(num_objects) == num_objects[0]
num_samples_per_class = num_objects[0]
b1 = int(np.ceil(TRAIN_SPLIT * num_samples_per_class))

# split objects with corresponding signals keeping 80/10/10 split sizes equally distributed among each object
train_dataset, val_dataset, test_dataset = dict.fromkeys(dataset, list()), \
                                           dict.fromkeys(dataset, list()), \
                                           dict.fromkeys(dataset, list())

# populate dataset dicts
for key in dataset.keys():
    train, test = list(), list()
    shuffled = False
    indices_train, indices_test = None, None
    for i, sample in enumerate(dataset[key]):

        # each object is sampled the same way with the same seed
        random.seed(i)

        sample = list(sample)
        random.shuffle(sample)
        train.append(sample[:b1])
        test.append(sample[b1:])

    # populate new datasets
    train, test = np.array(train), np.array(test)
    train = train.reshape(train.shape[0] * train.shape[1], *train.shape[2:])
    test = test.reshape(test.shape[0] * test.shape[1], *test.shape[2:])
    train_dataset[key], test_dataset[key] = train, test


# save datasets
def save_as_pickle(dict_to_save, file_name):
    with open(file_name, 'wb') as f:
        pickle.dump(dict_to_save, f)


save_as_pickle(train_dataset, "datasets/bolt/train.pickle")
save_as_pickle(test_dataset, "datasets/bolt/test.pickle")

# ########################## DISPLAY ALL IMAGES AND SIGNALS WITH ADJECTIVES
# # print barplot - comment to proceed
# all_adjectives_names = np.hstack(dataset["labels_str"])
# unique_adjectives_names, occurances = np.unique(all_adjectives_names, return_counts=True)
# plt.bar(unique_adjectives_names, occurances)
# plt.xticks(rotation='vertical')
# plt.show()
#
# # iterate over dataset samples
# paths = dataset["images_paths"]
# plt.figure(figsize=(10, 6))
# for i, path in enumerate(paths):
#     img = PIL.Image.open(path)
#     electrodes_0, electrodes_1 = dataset["electrodes_0"][i], dataset["electrodes_1"][i]
#     pac_0, pac_1 = dataset["pac_0"][i], dataset["pac_1"][i]
#     pad_0, pad_1 = dataset["pad_0"][i], dataset["pad_1"][i]
#     tac_0, tac_1 = dataset["tac_0"][i], dataset["tac_1"][i]
#     tad_0, tad_1 = dataset["tad_0"][i], dataset["tad_1"][i]
#     adjectives_str = dataset["labels_str"][i]
#     adjectives_num = dataset["labels_num"][i]
#     assert len(adjectives_str) == len(adjectives_num)
#
#     # display image and plot signals
#     plt.subplot(6, 2, 1)
#     plt.plot(electrodes_0)
#     plt.title("electrodes_0")
#     plt.subplot(6, 2, 2)
#     plt.plot(electrodes_1)
#     plt.title("electrodes_1")
#
#     plt.subplot(6, 2, 3)
#     plt.plot(pac_0)
#     plt.title("pac_0")
#     plt.subplot(6, 2, 4)
#     plt.plot(pac_1)
#     plt.title("pac_1")
#
#     plt.subplot(6, 2, 5)
#     plt.plot(pad_0)
#     plt.title("pad_0")
#     plt.subplot(6, 2, 6)
#     plt.plot(pad_1)
#     plt.title("pad_1")
#
#     plt.subplot(6, 2, 7)
#     plt.plot(tac_0)
#     plt.title("tac_0")
#     plt.subplot(6, 2, 8)
#     plt.plot(tac_1)
#     plt.title("tac_1")
#
#     plt.subplot(6, 2, 9)
#     plt.plot(tad_0)
#     plt.title("tad_0")
#     plt.subplot(6, 2, 10)
#     plt.plot(tad_1)
#     plt.title("tad_1")
#
#     plt.subplot(6, 1, 1)
#     plt.imshow(img)
#     plt.title("Object")
#
#     plt.show(block=False)
#     plt.pause(1)
#     plt.clf()
#
#     # print names and indexes
#     print("\n\nExample no. {}".format(i))
#     [print("{}: {}".format(adj, index)) for adj, index in zip(adjectives_str, adjectives_num)]
